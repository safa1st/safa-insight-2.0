<?php

use Illuminate\Database\Seeder;

class TurnoverPercentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear all data
        DB::table('turnover_percent')->delete();

        // Default
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'DEFAULT',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );

        // California
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'CA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );

        // Arizona
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'AZ',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );

        // Texas
        // California
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'TX',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );

        // Illinois
        // California
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'IL',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );

        // Massachusetts
        // California
        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'millennials',
                'replacement_cost' => 1.07
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen x',
                'replacement_cost' => 1.60
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'baby boomer',
                'replacement_cost' => 2.13
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'executive',
                'generation' => 'gen z',
                'replacement_cost' => .53
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'millennials',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen x',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'baby boomer',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'management',
                'generation' => 'gen z',
                'replacement_cost' => .2
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'millennials',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen x',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'baby boomer',
                'replacement_cost' => .16
            ]
        );

        DB::table('turnover_percent')->insert(
            [
                'state_code' => 'MA',
                'year' => '2018', 
                'job_category' => 'staff',
                'generation' => 'gen z',
                'replacement_cost' => .16
            ]
        );
    }
}
