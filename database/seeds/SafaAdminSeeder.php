<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SafaAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userid = DB::table('users')->insertGetId(
            [
                'name' =>'Safa Admin', 
                'email' =>'admin@safa.io',
                'password' =>'$2y$10$9R5Y0czXDKG.J9ZA/8Er7u50cxf9qNe9b7/rWyNKmR5GFfkJMiVa6'
            ]
        );

        $teamid = DB::table('teams')->insertGetId(
            [
                'owner_id' => $userid, 
                'name' =>'Safa Team'
            ]
        );

        DB::table('team_users')->insert(
            [
                'team_id' => $teamid, 
                'user_id' => $userid,
                'role' => 'owner'
            ]
        );
    }
}
