DO $$
DECLARE
    user_name varchar(255) := 'Bob Smith';
    user_email varchar(255) := 'bob@eb.com';
    team_name varchar(255) := 'Eastern Benefits';
    user_id bigint;
    team_id bigint;

BEGIN

INSERT INTO users(name, email, password, created_at, updated_at)
VALUES (user_name, user_email, '$2y$10$zrcoNs4ATdOabrF21RudCOANP9q9y3RQLaRlNZVgdI1jN5YCtpFtO', current_timestamp, current_timestamp)
RETURNING id INTO user_id;

INSERT INTO teams(owner_id, name)
VALUES (user_id, team_name)
RETURNING id INTO team_id;

INSERT INTO team_users(team_id, user_id, role)
VALUES (team_id, user_id, 'owner');

END $$;
