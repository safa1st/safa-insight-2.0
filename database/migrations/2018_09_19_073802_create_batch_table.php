<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('client_id')->index();
            $table->string('title', 1000)->nullable();
            $table->json('file');
            $table->unsignedInteger('batch_type_lid')->index();
            $table->boolean('deleted_flag')->nullable();
            $table->timestamps();
        });

        Schema::table('batch', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('batch_type_lid')->references('id')->on('lists');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch');
    }
}
