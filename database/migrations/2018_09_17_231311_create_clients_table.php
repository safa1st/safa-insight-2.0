<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->string('name', 100);
            $table->string('industry', 50);
            $table->string('size', 50);
            $table->string('address', 255);
            $table->string('address2', 255)->nullable();
            $table->string('city', 255);
            $table->string('state', 3);
            $table->string('zipcode', 10);
            $table->string('contact_name', 50);
            $table->string('contact_email', 50);
            $table->string('contact_work_phone', 20);
            $table->string('contact_mobile_phone', 20);
            $table->string('logo_path', 100)->nullable();
            $table->string('logo_mime_type', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
