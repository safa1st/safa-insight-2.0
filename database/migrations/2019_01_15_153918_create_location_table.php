<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_code');
            $table->string('country_name');
            $table->string('state_code');
            $table->string('state_name');
            $table->string('flag_path');
        });

        // Source: https://countrycode.org
        // Source: https://www.states101.com/flags
        DB::table('location')->insert(
            array (
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'AL', 'state_name' =>'Alabama', 'flag_path' => 'flags/us_states/alabama.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'AK', 'state_name' =>'Alaska', 'flag_path' => 'flags/us_states/alaska.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'AZ', 'state_name' =>'Arizona', 'flag_path' => 'flags/us_states/arizona.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'AR', 'state_name' =>'Arkansas', 'flag_path' => 'flags/us_states/arkansas.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'CA', 'state_name' =>'California', 'flag_path' => 'flags/us_states/california.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'CO', 'state_name' =>'Colorado', 'flag_path' => 'flags/us_states/colorado.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'CT', 'state_name' =>'Connecticut', 'flag_path' => 'flags/us_states/connecticut.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'DE', 'state_name' =>'Delaware', 'flag_path' => 'flags/us_states/delaware.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'DC', 'state_name' =>'District of Columbia', 'flag_path' => 'flags/us_states/district-of-columbia.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'FL', 'state_name' =>'Florida', 'flag_path' => 'flags/us_states/florida.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'GA', 'state_name' =>'Georgia', 'flag_path' => 'flags/us_states/georgia.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'HI', 'state_name' =>'Hawaii', 'flag_path' => 'flags/us_states/hawaii.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'ID', 'state_name' =>'Idaho', 'flag_path' => 'flags/us_states/idaho.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'IL', 'state_name' =>'Illinois', 'flag_path' => 'flags/us_states/illinois.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'IN', 'state_name' =>'Indiana', 'flag_path' => 'flags/us_states/indiana.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'IA', 'state_name' =>'Iowa', 'flag_path' => 'flags/us_states/iowa.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'KS', 'state_name' =>'Kansas', 'flag_path' => 'flags/us_states/kansas.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'KY', 'state_name' =>'Kentucky', 'flag_path' => 'flags/us_states/kentucky.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'LA', 'state_name' =>'Louisiana', 'flag_path' => 'flags/us_states/louisiana.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'ME', 'state_name' =>'Maine', 'flag_path' => 'flags/us_states/maine.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MD', 'state_name' =>'Maryland', 'flag_path' => 'flags/us_states/maryland.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MA', 'state_name' =>'Massachusetts', 'flag_path' => 'flags/us_states/massachusetts.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MI', 'state_name' =>'Michigan', 'flag_path' => 'flags/us_states/michigan.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MN', 'state_name' =>'Minnesota', 'flag_path' => 'flags/us_states/minnesota.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MS', 'state_name' =>'Mississippi', 'flag_path' => 'flags/us_states/mississippi.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MO', 'state_name' =>'Missouri', 'flag_path' => 'flags/us_states/missouri.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'MT', 'state_name' =>'Montana', 'flag_path' => 'flags/us_states/montana.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NE', 'state_name' =>'Nebraska', 'flag_path' => 'flags/us_states/nebraska.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NV', 'state_name' =>'Nevada', 'flag_path' => 'flags/us_states/nevada.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NH', 'state_name' =>'New Hampshire', 'flag_path' => 'flags/us_states/new-hampshire.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NJ', 'state_name' =>'New Jersey', 'flag_path' => 'flags/us_states/new-jersey.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NM', 'state_name' =>'New Mexico', 'flag_path' => 'flags/us_states/new-mexico.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NY', 'state_name' =>'New York', 'flag_path' => 'flags/us_states/new-york.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'NC', 'state_name' =>'North Carolina', 'flag_path' => 'flags/us_states/north-carolina.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'ND', 'state_name' =>'North Dakota', 'flag_path' => 'flags/us_states/north-dakota.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'OH', 'state_name' =>'Ohio', 'flag_path' => 'flags/us_states/ohio.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'OK', 'state_name' =>'Oklahoma', 'flag_path' => 'flags/us_states/oklahoma.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'OR', 'state_name' =>'Oregon', 'flag_path' => 'flags/us_states/oregon.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'PA', 'state_name' =>'Pennsylvania', 'flag_path' => 'flags/us_states/pennsylvania.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'RI', 'state_name' =>'Rhode Island', 'flag_path' => 'flags/us_states/rhode-island.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'SC', 'state_name' =>'South Carolina', 'flag_path' => 'flags/us_states/south-carolina.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'SD', 'state_name' =>'South Dakota', 'flag_path' => 'flags/us_states/south-dakota.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'TN', 'state_name' =>'Tennessee', 'flag_path' => 'flags/us_states/tennessee.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'TX', 'state_name' =>'Texas', 'flag_path' => 'flags/us_states/texas.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'UT', 'state_name' =>'Utah', 'flag_path' => 'flags/us_states/utah.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'VT', 'state_name' =>'Vermont', 'flag_path' => 'flags/us_states/vermont.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'VA', 'state_name' =>'Virginia', 'flag_path' => 'flags/us_states/virgina.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'WA', 'state_name' =>'Washington', 'flag_path' => 'flags/us_states/washington.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'WV', 'state_name' =>'West Virginia', 'flag_path' => 'flags/us_states/west-virginia.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'WI', 'state_name' =>'Wisconsin', 'flag_path' => 'flags/us_states/wisconsin.svg'),
                array ('country_code' =>'US', 'country_name' =>'United States', 'state_code' =>'WY', 'state_name' =>'Wyoming', 'flag_path' => 'flags/us_states/wyoming.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'AB', 'state_name' =>'Alberta', 'flag_path' => 'flags/ca_provinces/CA-AB.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'BC', 'state_name' =>'British Columbia', 'flag_path' => 'flags/ca_provinces/CA-BC.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'MB', 'state_name' =>'Manitoba', 'flag_path' => 'flags/ca_provinces/CA-MB.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'NB', 'state_name' =>'New Brunswick', 'flag_path' => 'flags/ca_provinces/CA-NB.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'NL', 'state_name' =>'Newfoundland and Labrador', 'flag_path' => 'flags/ca_provinces/CA-NL.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'NT', 'state_name' =>'Northwest Territories', 'flag_path' => 'flags/ca_provinces/CA-NT.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'NS', 'state_name' =>'Nova Scotia', 'flag_path' => 'flags/ca_provinces/CA-NS.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'NU', 'state_name' =>'Nunavut', 'flag_path' => 'flags/ca_provinces/CA-NU.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'ON', 'state_name' =>'Ontario', 'flag_path' => 'flags/ca_provinces/CA-ON.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'PE', 'state_name' =>'Prince Edward Island', 'flag_path' => 'flags/ca_provinces/CA-PE.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'QC', 'state_name' =>'Quebec', 'flag_path' => 'flags/ca_provinces/CA-QC.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'SK', 'state_name' =>'Saskatchewan', 'flag_path' => 'flags/ca_provinces/CA-SK.svg'),
                array ('country_code' =>'CA', 'country_name' =>'Canada', 'state_code' =>'YT', 'state_name' =>'Yukon', 'flag_path' => 'flags/ca_provinces/CA-YT.svg')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
