<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 100);
            $table->string('code', 8);
            $table->string('name', 1000);
            $table->unique(['type', 'code']);
        });

        DB::table('lists')->insert(
            array (
                array ('type' =>'batch_type', 'code' =>'WP', 'name' =>'Workforce Profile'),
                array ('type' =>'batch_type', 'code' =>'WPU', 'name' =>'Workforce Profile Unknown Format'),
                array ('type' =>'job_category', 'code' =>'E', 'name' =>'Executive'),
                array ('type' =>'job_category', 'code' =>'M', 'name' =>'Management'),
                array ('type' =>'job_category', 'code' =>'S', 'name' =>'Staff')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lists');
    }
}
