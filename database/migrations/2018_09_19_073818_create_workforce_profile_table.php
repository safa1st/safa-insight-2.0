<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkforceProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workforce_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('client_id')->index();
            $table->unsignedInteger('batch_id')->nullable()->index();
            $table->string('identifier');
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('gender', 16);
            $table->date('dob');
            $table->string('address1', 1000)->nullable();
            $table->string('address2', 1000)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('zip', 100)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('job_category')->nullable();
            $table->string('job_title')->nullable();
            $table->decimal('annual_salary')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('associations')->nullable();
            $table->boolean('deleted_flag')->nullable();
            $table->timestamps();
        });

        Schema::table('workforce_profile', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workforce_profile');
    }
}
