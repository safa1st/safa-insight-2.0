<ul class="nav flex-column">
  <li class="nav-item">
    <button class="btn btn-light btn-circle"><a href="/home"><i class="fa fa-home"></i></a></button>
  </li>
  <li class="nav-item">
    <select-client-nav-item
      display-text='Select Company'
      base-redirect-url="{{ $client_dashboard_url }}"/>
  </li>
  <li class="nav-item">
    <button class="btn btn-light btn-circle"><a href="/home/clients"><i class="fa fa-plus"></i></a></button>
  </li>
</ul>
