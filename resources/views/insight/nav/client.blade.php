<div>
  <div style="width: 60px;
    height: 60px;
    background-image: url('{{ $logo_url }}');
    background-size: cover;
    margin: 0px auto 20px auto
    padding: 0 0 20px 0;">
  </div>
  <ul class="nav flex-column">
    <li class="nav-item"><a href="{{ $present_url }}" class="nav-link"><i class="fa fa-television"></i> Present</a></li>
    <li class="nav-item"><a href="{{ $edit_url }}" class="nav-link"><i class="fa fa-pencil-square-o"></i> Edit</a></li>
    <li class="nav-item"><a href="{{ $wp_url }}" class="nav-link"><i class="fa fa-users"></i> Workforce</a></li>
  </ul>
</div>
