@extends('insight.layouts.dashboard.client')

@section('safa-content')

<div class="row">
  <div class="col-sm-6">
    <h3>Edit Company Details</h3>
  </div>
  <div class="col-sm-6" style="text-align: right">
    <delete-client :client-delete-url="'{{ $client_delete_url }}'" />
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <view-edit-client :client-data='{{ json_encode($client_resource) }}' />
  </div>
</div>

@endsection
