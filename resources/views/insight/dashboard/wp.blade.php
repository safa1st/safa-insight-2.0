@extends('insight.layouts.dashboard.client')

@section('safa-content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="module">
<h3>Workforce Profile Upload History</h3>
  @if(count($dataset) > 0)
  <table id="wp_list" class="table table-sm report">
          <thead>
              <th>Title</th>
              <th>Date Uploaded</th>
              <th>Total Records</th>
              <th class="th-fit"></th>
          </thead>

          <tbody>
              @foreach($dataset as $record)
                  <tr>
                      <td>
                            {{ $record->title }}
                      </td>

                      <td>
                              {{ $record->created_at }}
                      </td>

                      <td>
                              <a href="{{ route('insight.dashboard.wp.list', [$record->client_id, $record->id]) }}">{{ $record->total_records }}</a>
                      </td>

                      <td class="td-fit">
                          <a href="#" onclick="if (confirm('Are you sure to delete this set of data?')) deleteBatch({{ $record->client_id }}, {{ $record->id }});" class="btn btn-sm btn-default">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                      </td>
                  </tr>
              @endforeach
          </tbody>
      </table>
  @else
      <h6>You have no imported datasets.</h6>
  @endif

  <!-- flash message service -->
  @if(Session::has('flash_message'))
      <div class="alert {{ session('flash_type') }}" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>
          {!! session('flash_message') !!}
      </div>
  @endif

  <!-- validation errors user feedback -->
  @if(count($errors))
      <div class="alert alert-danger" role="alert">
          <ul class="mb-0">
              @foreach($errors->all() as $error)
                  <li>{!! $error  !!} </li>
              @endforeach
          </ul>
      </div>
  @endif


</div>
  <form enctype="multipart/form-data" method="post" action="{{ $wp_upload_url }}" onSubmit="return validateUploadForm()">
        {{csrf_field()}}
  <div class="module">
  <h4>Add New Workforce Profile Data File</h4>
  <div id="upload_file_errors" class="alert alert-danger" role="alert" style="display:none">
      <ul class="mb-0">
        <li id="upload_file_errors_title">The title field is required.</li>
        <li id="upload_file_errors_file">The upload field is required.</li>
      </ul>
  </div>
  <label for="title">Title</label>
  <input type="text" name="title" id="upload_file_title" class="form-control">
  <div class="form-group">
    <label for="upload">Upload File <br/>
        <a href="/files/wp_csv_spec_lite.pdf" target="_blank">Read File Format Specification</a><br/>
        <a href="/files/wp_sample.csv" target="_blank">Download Sample CSV File</a>
    </label>
    <input type="file" class="form-control-file" id="upload_file" name="upload">
  </div>
  <input type="hidden" name="client_id" value="{{ $client_id }}">
  <button type="submit" class="btn btn-primary">Upload</button>
  </div>
</form>
@endsection

@push('scripts')
<script>
    function validateUploadForm(){

      var isValid = true;
      var nameEl = $('#upload_file_title');
      var fileEl = $('#upload_file');
      var validationBlock = $('#upload_file_errors');
      var titleRequiredMessageEl = $('#upload_file_errors_title');
      var fileRequiredMessageEl = $('#upload_file_errors_file');
      validationBlock.hide();
      titleRequiredMessageEl.hide();
      fileRequiredMessageEl.hide();
      nameEl.removeClass('is-invalid');
      fileEl.css('border', '');

      if (nameEl.val().trim().length === 0){
        nameEl.addClass('is-invalid');
        validationBlock.show();
        titleRequiredMessageEl.show();
        isValid = false;
      }
      else{
        titleRequiredMessageEl.hide();
      }

      if (fileEl.val().trim().length === 0){
        fileEl.css({'border': '1px solid #dc3545'});
        validationBlock.show();
        fileRequiredMessageEl.show();
        isValid = false;
      }
      else{
        fileRequiredMessageEl.hide();
      }


      return isValid;
    }

    function deleteBatch(client_id, batch_id)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ $wp_delete_url }}',
            type: "POST",
            data: {
                _token : $('meta[name="csrf-token"]').attr('content'),
                client_id: client_id,
                batch_id: batch_id
            },
            dataType: "json",
            success:function(data) {
                //alert(data.msg);
                if (data.status == "0")
                    location.reload();
            },
            error: function(data) {
                alert(data.msg);
            },
        });
    }
</script>

@endpush
