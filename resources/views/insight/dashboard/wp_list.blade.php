@extends('insight.layouts.dashboard.client')

@section('safa-content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="card card-default">
                <div class="card-header">
                    @if(count($dataset) > 0)
                        {{array_values($dataset)[0]->title}} Listing
                    @else
                        Batch Listing
                    @endif
                </div>
              <div class="card-body">
                  @if(count($dataset) > 0)
                    <table id="wp_list" class="table table-striped report">
                        <thead>
                            <th>Id</th>
                            <th>Sex</th>
                            <th>Birthdate</th>
                            <th>Generation</th>
                            <th>Job Category</th>
                            <th>Job Title</th>
                            <th>Salary</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </thead>

                        <tbody>
                            @foreach($dataset as $record)
                                <tr>
                                    <td>
                                        {{ $record->identifier }}
                                    </td>
                                    <td>
                                        {{ $record->gender }}
                                    </td>
                                    <td>
                                        {{$record->dob}}
                                        <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="{{ $record->generation }}"></i>
                                    </td>
                                    <td>
                                        {{$record->generation}}
                                    </td>
                                    <td>
                                            {{ucwords($record->job_category)}}
                                            @if($record->job_title)
                                            <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="{{ $record->job_title }}"></i>
                                            @endif
                                    </td>
                                    <td>
                                        {{ $record->job_title }}
                                    </td>
                                    <td>
                                        ${{ number_format($record->annual_salary) }}
                                    </td>
                                    <td>
                                        {{$record->start_date}}
                                    </td>
                                    <td>
                                        {{$record->end_date}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h6>You have no imported datasets.</h6>
                @endif
              </div>
          </div>
      </div>
    </div>
</div>
@endsection

@push('css')
    <link href="{{ mix('css/bundle.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script>
           $(document).ready( function () {
            $('[data-toggle="tooltip"]').tooltip();

			$('#wp_list')
				.addClass( 'nowrap' )
				.dataTable( {
					responsive: false,
					"columnDefs": [
                        {
                            orderable: true,
                            "visible": false, "targets": [3, 5]
                        }
                    ],
                    "order": [[0, "asc"]]
                } );
        } );
        
    </script>
@endpush
