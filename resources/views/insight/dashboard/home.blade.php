@extends('insight.layouts.dashboard.home')

@section('safa-content')
<div class="title-wrapper">
  <h3>Your Companies</h3>
  <div class="title-actions">
    <ul class="nav">
      <li class="nav-item">
        <a href="{{ $create_client_url }}" class="btn btn-primary">Add New Company</a>
      </li>
    </ul>
  </div>
</div>
<table class="table">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Company</th>
      <th scope="col">Contact</th>
    </tr>
  </thead>
  <tbody>
    @foreach($clients as $client)
    <tr class="clickable-row" data-href="{{ $client->homeURL }}">
      <td><img src="{{ $client->company_logo }}" class="thumbnail-circle company-thumbnail" onerror="this.src='/img/company_logo_missing.png'" /></td>
      <th scope="row"><a href="{{ $client->homeURL }}">{{ $client->name }}</a></th>
      <td>{{ $client->contact_name }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@push('scripts')
    <script>
      $(document).ready(function($) {
        $(".clickable-row").click(function() {
          window.location = $(this).data("href");
        });
      });
    </script>
@endpush
