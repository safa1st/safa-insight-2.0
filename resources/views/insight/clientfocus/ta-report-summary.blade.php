@extends('insight.layouts.clientfocus')

@section('safa-content')
<script>
    var total_replacement_cost = 0;
    var avg_turnover_percent = 0;

    function addReplacementCost(replacement_cost)
    {
        if (!replacement_cost)
            replacement_cost = 0;
        total_replacement_cost += replacement_cost;
    }

    function addTurnoverPercent(turnover_percent)
    {
        if (!turnover_percent)
            turnover_percent = 0;
        avg_turnover_percent += turnover_percent;
    }
</script>
<div class="container-fluid">
<h3>Turnover Analysis</h3>

<table style="padding-spacing: 0px; border-spacing: 0px; border-collapse: separate;">
    <tr style="height: 30px;">
        <td style="width: 160px; white-space: nowrap; text-align: center; border-top: 2px solid #E1E1E1; border-left: 2px solid #E1E1E1; border-right: 2px solid #E1E1E1">
                <a href="{{ $ta_summary_url }}" class="nav-link disabled" style="font-weight: 600;">Summary Report</a>
        </td>
        <td style="width: 160px; white-space: nowrap; text-align: center; border-bottom: 2px solid #E1E1E1;">
                <a href="{{ $ta_url }}" class="nav-link">Report By Year</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 10px; border-bottom: 2px solid #E1E1E1; border-left: 2px solid #E1E1E1; border-right: 2px solid #E1E1E1;">
                @if(isset($start_date) > 0)
                {{Form::open(array('method' => 'post', 'route' => ['insight.clientfocus.ta.report.summary', $client_id], 'onsubmit' => 'return ValidateDateRange()'))}}
                {{ Form::hidden('client_id', $client_id) }}
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>Start Date: </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" placeholder="mm/dd/yyyy" autocomplete="off" id="start_date" name="start_date" class="form-control" style="width: 107px" value="{{$start_date}}">
                                            @if(!preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']) && !preg_match('/Trident\/7.0; rv:11.0/', $_SERVER['HTTP_USER_AGENT']))
                                                <span class="dtspan">
                                                    <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                                </span>
                                            @endif
                                        </div>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>End Date: </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" placeholder="mm/dd/yyyy" autocomplete="off" id="end_date" name="end_date" class="form-control" style="width: 107px" value="{{$end_date}}">
                                            @if(!preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']) && !preg_match('/Trident\/7.0; rv:11.0/', $_SERVER['HTTP_USER_AGENT']))
                                                <span class="dtspan">
                                                    <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                                </span>
                                            @endif
                                        </div>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td><button type="submit" class="form-control">Go</button></td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            @if($state == "DEFAULT")
                                <h6>Reporting is based on our default benchmarking data.</h6>
                            @else
                                @if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']) || preg_match('/Trident\/7.0; rv:11.0/', $_SERVER['HTTP_USER_AGENT']))
                                    <h6>Reporting is based on benchmarking data for the state of {{$state}}</h6>
                                @else
                                    <img src="/{{$flag_path}}" width="75" style="border-style: solid; border-width: 1px; border-color: #C1C1C1" data-toggle="tooltip" title="Reporting is based on benchmarking data for the state of {{$state}}"/>
                                @endif  
                            @endif
                        </td>
                    </tr>
                </table>
                
                {{ Form::close() }}
                <br/>
                <table style="width: 900px">
                    <tr>
                        <td class="align-top" style="width: 700px">
                            <table class="no-padding" id="ta-year-container">
                                <tbody style="background-color: #F4F5F7;" id="ta-year-body" data-parent="#ta-year-container" class="show">
                                    <tr style="height: 30px; background-color: #E1E1E1;">
                                        <td style="font-weight: 600; width: 120px; white-space: nowrap">
                                            &nbsp;&nbsp;Employee Type
                                        </td>
                                        <td style="font-weight: 600; width: 160px; white-space: nowrap">
                                            Avg Replacement Cost
                                        </td>
                                        <td style="font-weight: 600; width: 90px; white-space: nowrap">
                                            Turnover
                                        </td>
                                        <td style="font-weight: 600; width: 120px; white-space: nowrap">
                                            Your Workforce
                                        </td>
                                        <td style="font-weight: 600; width: 170px; white-space: nowrap">
                                            Actual Replacement Cost
                                        </td>
                                    </tr>
                                    <!-- Overall -->
                                    @php ($row = $ta_dataset['overall'])
                                    <tr style="height: 30px;">
                                        <td style="font-weight: 600; white-space: nowrap">
                                            &nbsp;&nbsp;{{ ucwords($row->job_category) }}
                                        </td>
                                        <td style="font-weight: 600;">
                                            ${{ number_format($row->avg_replacement_cost) }}
                                        </td>
                                        <td style="font-weight: 600; white-space: nowrap">
                                            {{ number_format($row->turnover_percent * 100, 2) }}%
                                            ({{ number_format($row->turnover_count) }})
                                            <script>addTurnoverPercent({{$row->turnover_percent}});</script>
                                        </td>
                                        <td style="font-weight: 600;">
                                            {{ number_format($row->employee_count) }}
                                        </td>
                                        <td style="font-weight: 600;">
                                            ${{ number_format($row->replacement_cost) }}
                                            <script>addReplacementCost({{$row->replacement_cost}});</script>
                                        </td>
                                    </tr>
                
                                    <!-- Job Type Overall -->
                                    <tr style="height: 30px;">
                                        <td class="accordion-body" colspan="5">
                                            @foreach($job_type_dataset as $job_type)
                                                @if(isset($ta_dataset[$job_type]))
                                                    @php ($row = $ta_dataset[$job_type])
                                                @else
                                                    @php ($row = null)
                                                @endif
                                                @if($row)
                                                    <table class="table-striped table-borderless no-padding" id="ta-jc-container-{{$row->job_category}}">
                                                        <thead>
                                                            <th style="width: 120px; height: 30px; ">
                                                                &nbsp;
                                                                <span class="panel-title">
                                                                    <a class="collapsed expand-grp" data-toggle="collapse" data-parent="#ta-jc-container-{{$row->job_category}}" href="#ta-jc-body-{{$row->job_category}}" aria-expanded="false" aria-controls="ta-jc-body-{{$row->job_category}}">
                                                                    {{ ucwords($row->job_category) }}
                                                                    </a>
                                                                </span>
                                                            </th>
                                                            <th style="width: 160px">
                                                                ${{ number_format($row->avg_replacement_cost) }}
                                                            </th>
                                                            <th style="width: 90px">
                                                                {{ number_format($row->turnover_percent * 100, 2) }}%
                                                                ({{ number_format($row->turnover_count) }})
                                                            </th>
                                                            <th style="width: 120px">
                                                                {{ number_format($row->employee_count) }}
                                                            </th>
                                                            <th style="width: 170px">
                                                                ${{ number_format($row->replacement_cost) }}
                                                            </th>
                                                        </thead>
                                                        <tbody class="accordion-body collapse" id="ta-jc-body-{{$row->job_category}}">
                                                            @foreach($gen_dataset as $key => $value)
                
                                                                @if(isset($ta_dataset[$job_type.$key]))
                                                                    @php ($row = $ta_dataset[$job_type.$key])
                                                                @else
                                                                    @php ($row = null)
                                                                @endif
                                                                @if($row)
                                                                <tr style="height: 30px;">
                                                                    <td style="white-space: nowrap">
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ucwords($row->generation) }}
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="white-space: nowrap">
                                                                        {{ number_format($row->turnover_percent * 100, 2) }}%
                                                                        ({{ number_format($row->turnover_count) }})
                                                                    </td>
                                                                    <td>
                                                                        {{ number_format($row->employee_count) }}
                                                                    </td>
                                                                    <td>
                                                                        ${{ number_format($row->replacement_cost) }}
                                                                    </td>
                                                                </tr>
                                                                @else
                                                                <tr style="height: 30px;">
                                                                    <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$gen_dataset[$key]}}</td><td>&nbsp;</td><td>-</td><td>-</td><td>-</td>
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    
                                                    @endif
                
                                            @endforeach
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td valign="middle">
                                        Total Replacement Cost:
                                        <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="This number represents the total cost of turnover for the date range selected"></i>
                                        <div id="rcs" style="font-size: 18pt; color: gray}}">
                                        </div>
                
                                        <br/>
                                        Turnover %:
                                        <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="This number represents the average turnover percentage for the date range selected"></i>
                                        <div id="rrc" style="font-size: 18pt; color: gray}}">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
                @else
                    No Data Imported
                @endif  
        </td>
    </tr>
</table>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


<div class="prev-button">
  <a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
</div>
<div class="next-button">
  <a href="../td"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
</div>

@endsection

@push('css')
<link rel="stylesheet" href="/css/jquery-ui.css">
<style TYPE="text/css">
th {
    font-weight: 400;
}

.panel-title > a:before {
    font-family: FontAwesome;
    content:"\f068";
    padding-right: 5px;
}
.panel-title > a.collapsed:before {
    content:"\f067";
}
.panel-title > a:hover,
.panel-title > a:active,
.panel-title > a:focus  {
    text-decoration:none;
}

.expand-grp
{
    color: #424A55;
}

.no-padding
{
    padding: 0 !important;
    margin: 0 !important;
}

/* Tooltip container */
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
}

/* Tooltip text */
.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  padding: 5px 0;
  border-radius: 6px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 1;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip:hover .tooltiptext {
  visibility: visible;
}

.dtspan{
  position: absolute;
  margin-left: 86px;
  margin-top: 2px;
  height: 25px;
  display: flex;
  align-items: center;
}
input{
  padding-right: 25px;
  height: 20px;
}
.fa-calendar {
    z-index: 99;
    color: #B1B1B1;
}
</style>

@endpush

@push('scripts')
<script src="/js/jquery-ui.js"></script>
<script>
    function isValidDate(dateString)
    {
        // First check for the pattern
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[1], 10);
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if(year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }

    function ValidateDateRange()
    {
        var startDate = $('#start_date').val();
        var endDate = $('#end_date').val();

        if (!startDate || !endDate)
        {
            alert("Invalid dates selected");
            return false;
        }

        try
        {
            if (!isValidDate(startDate))
            {
                alert("Start Date is not valid");
                return false;
            }
            else if (!isValidDate(endDate))
            {
                alert("End Date is not valid");
                return false;
            }
            if (new Date(startDate) > new Date(endDate))
            {
                alert("End Date cannot be before Start Date");
                return false;
            }
        }
        catch(err)
        {
            alert("Invalid dates selected");
            return false;
        }

        return true;
    }
    $(document).ready(function(){
        $('#rcs').html('$' + Number(total_replacement_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

        $('#rrc').html(Number((avg_turnover_percent * 100)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '%');

        $('[data-toggle="tooltip"]').tooltip();

        $( "#start_date" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        $( "#end_date" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
    });

</script>
@endpush
