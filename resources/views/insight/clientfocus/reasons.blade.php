@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('safa-content')
<div class="container-fluid">
<h3>Reasons for Employee Turnover</h3>
<svg id="reasonsImage" style="display: none; margin: 0px auto;" viewBox="0 0 520 400" preserveAspectRatio="xMinYMin meet">
	<defs>
		<mask id="mask1" x="0" y="0" width="100" height="100" >
			<rect x="100" y="0" width="400" height="320"
				style="stroke:none; fill: #ffffff"/>
		</mask>
		<mask id="mask2" x="0" y="0" width="100" height="100" >
			<rect x="0" y="0" width="400" height="340"
				style="stroke:none; fill: #ffffff"/>
		</mask>
    <filter id="f1" x="-20%" y="-20%" width="200%" height="200%">
      <feOffset result="offOut" in="SourceGraphic" dx="5" dy="5" />
      <feGaussianBlur result="blurOut" in="offOut" stdDeviation="10" />
      <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
    </filter>
  </defs>
	<g>
		<circle opacity="0.15" cx="280" fill="white" stroke="gray" stroke-width="3" cy="200" r="170" mask="url(#mask1)" />
		<circle opacity="0.08" cx="190" fill="white" stroke="gray" stroke-width="2" cy="210" r="170" mask="url(#mask2)"  />
		<circle opacity="0.08" cx="230" fill="white" stroke="gray" stroke-width="2" cy="220" r="100" />
		<circle opacity="0.1" cx="230" fill="white" stroke="gray" stroke-width="1" cy="220" r="110" stroke-dasharray="3,2" />
		<a xlink:href="#" class="btn-cta" data-content-id="0">
			<g class="reason-bubble">
				<circle cx="160" stroke="white" stroke-width="3" cy="300" r="50" fill="#0c7020" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="160" y="285" dy="0" text font-size="8px">
					<tspan x="160" dy="0">Non-competitive</tspan>
					<tspan x="160" dy="10">benefits and/or</tspan>
					<tspan x="160" dy="10">compensation</tspan>
					<tspan x="160" dy="10">compared to</tspan>
					<tspan x="160" dy="10">industry</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="1">
			<g class="reason-bubble">
				<circle cx="170" cy="100" r="45" fill="#205482" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="150" y="87" dy="0" text font-size="8px">
					<tspan x="170" dy="0">Poor and</tspan>
					<tspan x="170" dy="10">ineffective</tspan>
					<tspan x="170" dy="10">leadership or</tspan>
					<tspan x="170" dy="10">management</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="2">
			<g class="reason-bubble">
				<circle cx="46" cy="255" r="40" fill="#28471f" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="46" y="250" dy="0" text font-size="8px">
					<tspan x="46" dy="0">Feeling</tspan>
					<tspan x="46" dy="10">unappreciated</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="3">
			<g class="reason-bubble">
				<circle cx="55" cy="155" r="35" fill="#346189" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="55" y="150" dy="0" text font-size="8px">
					<tspan x="55" dy="0">Low job</tspan>
					<tspan x="55" dy="10">satisfaction</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="4">
			<g class="reason-bubble">
				<circle cx="255" cy="157" r="35" fill="#32154f" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="255" y="159" dy="0" text font-size="8px">
					<tspan x="255" dy="0">Workload</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="5">
			<g class="reason-bubble">
				<circle cx="65" cy="347" r="35" fill="#32154f" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="65" y="340" dy="0" text font-size="8px">
					<tspan x="65" dy="0">Conflicts</tspan>
					<tspan x="65" dy="10">with</tspan>
					<tspan x="65" dy="10">company</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="12">
			<g class="reason-bubble">
				<circle cx="285" cy="67" r="40" fill="#32154f" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="285" y="60" dy="0" text font-size="8px">
					<tspan x="285" dy="0">Conflicts with</tspan>
					<tspan x="285" dy="10">supervisors</tspan>
					<tspan x="285" dy="10">or managers</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="6">
			<g class="reason-bubble">
				<circle cx="355" cy="167" r="35" fill="#9e7a17" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="355" y="164" dy="0" text font-size="8px">
					<tspan x="355" dy="0">Job</tspan>
					<tspan x="355" dy="10">Mismatch</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="7">
			<g class="reason-bubble">
				<circle cx="325" cy="257" r="35" fill="#9e7a17" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="325" y="254" dy="0" text font-size="8px">
					<tspan x="325" dy="0">Personal</tspan>
					<tspan x="325" dy="10">Issues</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="8">
			<g class="reason-bubble">
				<circle cx="275" cy="347" r="35" fill="#194360" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="275" y="340" dy="0" text font-size="8px">
					<tspan x="275" dy="0">Internal</tspan>
					<tspan x="275" dy="10">pay equity</tspan>
					<tspan x="275" dy="10">issues</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="11">
			<g class="reason-bubble">
				<circle cx="455" cy="157" r="40" fill="#0b6828" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="455" y="152" dy="0" text font-size="8px">
					<tspan x="455" dy="0">Perceptions</tspan>
					<tspan x="455" dy="10">of unfair</tspan>
					<tspan x="455" dy="10">treatment</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="10">
			<g class="reason-bubble">
				<circle  cx="435" cy="257" r="40" fill="#0b6828" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family: 'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="435" y="252" dy="0" text font-size="8px">
					<tspan x="435" dy="0">Looking for</tspan>
					<tspan x="435" dy="10">more career</tspan>
					<tspan x="435" dy="10">advancement</tspan>
				</text>
			</g>
		</a>
		<a xlink:href="#" class="btn-cta" data-content-id="9">
			<g class="reason-bubble">
				<circle cx="140" cy="195" r="45" fill="#346189" stroke="white" stroke-width="3" filter="url(#f1)" />
				<text style="font-family:'sans-serif'; font-weight: bold" fill="white" text-anchor="middle"
					x="140" y="185" dy="0" text font-size="8px">
					<tspan x="140" dy="0">External</tspan>
					<tspan x="140" dy="10">factors beyond</tspan>
					<tspan x="140" dy="10">company's</tspan>
					<tspan x="140" dy="10">control</tspan>
				</text>
			</g>
		</a>
	</g>
</svg>
</div>

	<div class="prev-button">
		<a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
	</div>
	<div class="next-button">
		<a href="{{ $cost_url }}"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
	</div>

<div id="reasonsModal" class="modal hide" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="reasons-content">
	<div id="dialogcontent0">
		<div class="title">NON-COMPETITIVE BENEFITS AND/OR COMPENSATION COMPARED TO INDUSTRY</div>
		<div class="content">
			<p>Recent years of the progressive tech-age and changing society has taught us how to hustle, and with a growing awareness of self-actualization, workers are go-getters and are constantly seeking bigger and better opportunity. Employees are embracing change and can choose to pursue multiple sources of income, for example side gigs can be viewed as a lot safer than depending on a single job. Employees will be motivated to go elsewhere if they feel that their skills and abilities will be utilized and potentials recognized, all of which go hand in hand with better compensation and benefit packages.</p>
			<p>Employers are seeing a domino effect whereby if one employee leaves for better opportunity elsewhere within the industry, others follow suit. Turnover due to turnover occurs when the remaining staff jobs are affected by the loss of a co-worker, for example they are left to pick up the slack that the former employee left behind. Employers can contribute to losing employees through non-competitive benefits and/or compensation compared to industry by failing to offer development 		opportunities or higher pay that other companies do.</p>
		</div>
	</div>
	<div id="dialogcontent1">
		<div class="title">POOR MANAGEMENT</div>
		<div class="content">
			<p>	With a lack of sufficient leaders and strong management representation within the organization, the employer sets the employee up for failure by lacking the following:</p>
			<ul>
				<li>Decision-Making Ability</li>
				<li>Coaching and Feedback</li>
				<li>Top-Down Communication</li>
				<li>Leadership Presence</li>
				<li>Employee Retention Plan</li>
			</ul>
		</div>
	</div>
	<div id="dialogcontent2">
		<div class="title">FEELING UNAPPRECIATED</div>
		<div class="content">
			<p>When an employee feels unappreciated, the company is failing to offer them recognition. This leads to feeling undervalued, whereby little commitment can be expected of the employee. As we say, it’s a give and take. This happens when talented young people are not getting the development they want, and autonomy and independence on the job is lacking.</p>
			<p>An employer can contribute to their employees feeling unappreciated by working their employees to the bone and failing to recognize their hard work. A lack of verbal acknowledgement of a job well done or formal feedback through performance evaluations will continue the decreased employee morale. When company’s fail to offer positive reinforcement through recognition rewards, employees continue to feel undervalued and will be more likely to quit.</p>
		</div>
	</div>
	<div id="dialogcontent3">
		<div class="title">LOW JOB SATISFACTION</div>
		<div class="content">
			<p>Low job satisfaction exists when the employee feels as though their work is not making a difference. Employees may dread coming into work, feel bored or are left wanting more than “just a job”.</p>
			<p>Employers contribute to low job satisfaction by failing to recognize the warning signs or listen to their workforce. Ultimately, a lack of Employee Engagement has spiralling effects that go beyond surface level low job satisfaction, including lowered productivity and poor attendance.</p>
		</div>
	</div>
	<div id="dialogcontent4">
		<div class="title">WORKLOAD</div>
		<div class="content">
			<p>Being overworked for an employee means feeling that there aren’t enough hours in the day to do their job effectively. Being forced to prioritize efficiencies, employee can feel as though they have to let the ball drop in certain areas and do not have the time to build a sense of rapport with others.</p>
			<p>Through mismanaged workload, employers risk turnover by failing to recognize individual bandwidths and exposing their employees to situation that present high amounts of job stress. The tipping point of turnover typically occurs once more demands are being placed on already highly stressed individuals.</p>
		</div>
	</div>
	<div id="dialogcontent5">
		<div class="title">CONFLICTS WITH COMPANY</div>
		<div class="content">
			<p>When an employee conflicts with the company, their faith and confidence is shaken. The employee’s views, business strategy and/or core values do not align with the company, creating conflict. There is no constructive way to work towards a common goal if the employee and the employer do not see eye to eye. Behaviours that cause employee conflict with the company could be a lack of knowledge about the organization’s financial stability lack of perceived justice, the presence of unfair, rude behaviour or being poorly managed.</p>
			<p>The employer can contribute to conflict by fostering a toxic company culture, offering organizational instability, maintaining negative interactions with employees by not getting along with them, or failing to mend broken relationships.</p>
		</div>
	</div>
	<div id="dialogcontent6">
		<div class="title">JOB MISMATCH</div>
		<div class="content">
			<p>Job mismatch exists when the talents and efforts of the employee and the expectations of the company do not align. The employee may decide that the work isn’t something they want to do as they were given different work than what was expected from the hiring process, or not given sufficient training and support to perform well in their role.</p>
			<p>An employer contributes to job mismatch when they fail to ensure alignment during the recruitment and selection process, and furthermore fail to adequately train and support the employee to ensure proper job fit. By offering a job description that does not match the actual job, the employer fails to meet the employee’s expectations.</p>
		</div>
	</div>
	<div id="dialogcontent7">
		<div class="title">PERSONAL ISSUES</div>
		<div class="content">
			<p>Personal issues of employees exist through conflicting work relationships (personal and professional), inadequate people skills, and a lack of engagement and solution-seeking efforts. Employers can contribute to personal issues at work when they tolerate rude or ignorant behaviour, work-life imbalance or the unequal treatment of team members.</p>
		</div>
	</div>
	<div id="dialogcontent8">
		<div class="title">INTERNAL PAY EQUITY ISSUES</div>
		<div class="content">
			<p>Internal pay equity issues exist when the employer fails to offer promotion and pay to their staff through growth opportunities. Examples of this include paying employees below market value, having primarily minimum wage staff, offering very low salary increases or none at all, or having raises and promotions frozen through hard economic times. By hiring a majority of entry-level employees who do not plan to stay in the position very long (e.g.. call centers or fast food restaurants), employers can expect employees to eventually seek higher paying opportunity elsewhere. Employers are at a higher risk of losing their human capital when they let their competitors offer more money for similar jobs.</p>
		</div>
	</div>
	<div id="dialogcontent9">
		<div class="title">EXTERNAL FACTORS BEYOND COMPANY’S CONTROL</div>
		<div class="content">
			<p>External factors in play that can lead to turnover are sometimes beyond the company’s control. Although an employee may be satisfied with their current job, alternatives that offer a more tempting or preferred lifestyle ultimately wins. Examples include relocation of partner or spouse, the increase of entrepreneurship, side gigs, and small business ownership, the increase of virtual work opportunities, social media and the Internet influences, or the job-hopping tendencies of Generations Y and Z.</p>
		</div>
	</div>
	<div id="dialogcontent10">
		<div class="title">LOOKING FOR MORE CAREER ADVANCEMENT</div>
		<div class="content">
			<p>One of the most important key elements of employee satisfaction and retention at any organization is career advancement. Employees are motivated to stay with their company when a clear direction of growth opportunity is present. If opportunity to advance looks promising with commitment and consistent effort, employees are more likely to tough through hard times or stick around during periods of dissatisfaction knowing it will pay off in the end.</p>
			<p>An employer contributes to career advancement by recognizing the high achievers of their talent base and implement a succession plan to promote these potentials to open positions. A lack of upper-level jobs within a company can create a challenge as it hinders opportunity for employees to try new skills and gain added responsibility. This in turn can lead to employee frustration and high turnover as they look elsewhere for career fulfillment.</p>
		</div>
	</div>
	<div id="dialogcontent11">
		<div class="title">PERCEPTIONS OF UNFAIR TREATMENT</div>
		<div class="content">
			<p>An employer contributes to perceptions of unfair treatment by failing to promote and maintain respectful workplace practices throughout their company. Without clear procedures in place that outline employee conduct expectations, companies are at a higher risk of unfair and disrespectful treatment of others. Oftentimes, the presence of policies alone are not enough. When employers invest in putting policy into practice through appropriate training exercises, workshop, and available resources to address employee relation concerns, it sends a message to employees that ongoing respectful workplace efforts are in place to protect them.</p>
		</div>
	</div>
	<div id="dialogcontent12">
		<div class="title">CONFLICTS WITH SUPERVISORS OR MANAGERS</div>
		<div class="content">
			<p>An employer can find it challenging to maintain a harmonious work environment if they do not have proper conflict resolution management strategies in place. When conflicts arise, employers who do not act timely and efficiently to diffuse, maintain and resolve the conflict are at greater risk for turnover. An employee’s commitment to the company significantly decreases if they feel as though their concerns are put on the back burner or outright ignored. Companies with proactive measures in place to mitigate the risk of conflict through training is a good step in the right direction to provide supervisors and managers with the tools and knowledge on how to effectively deal with various work styles and personality clashes.</p>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script>

$( document ).ready(function() {


	$('#reasonsImage').css('display', 'block')
	var maxHeight = ($(window).height() - 280);
	var maxWidth = maxHeight * 1.3;
	$('#reasonsImage').css('max-height', maxHeight + 'px');
	$('#reasonsImage').css('max-width', maxWidth + 'px');

	$( window ).resize(function() {
	  var maxHeight = ($(window).height() - 280);
		var maxWidth = maxHeight * 1.3;
		$('#reasonsImage').css('max-height', maxHeight + 'px');
		$('#reasonsImage').css('max-width', maxWidth + 'px');
	});

	$('.btn-cta').click(function (e) {
		e.preventDefault();
		var contentId = e.currentTarget.dataset["contentId"];
		var content = $('#dialogcontent' + contentId + " > div.content");
		var title = $('#dialogcontent' + contentId + " > div.title");

		var modal = $('#reasonsModal');
		modal.find('.modal-body').html(content.html());
		modal.find('.modal-title').html(title.html());
		$('#reasonsModal').modal('show');
  });
});

</script>
@endpush
