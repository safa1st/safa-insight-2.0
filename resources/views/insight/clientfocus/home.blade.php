@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('body_class', 'clientfocus clientfocus__home')

@section('safa-content')
  <div class="container-fluid h-100">

    <div class="row h-100">
  		<div class="col-sm-4 h-100">
  			<div class="welcome-safa-insight-bar">
  				<img src="{{asset('/img/safa-insight-logo-white.png')}}" width="250" />
  				
  			</div>
  		</div>
      <main class="col-sm-8 pt-3">
        <div class="container welcome-safa-customer">
  				<h4>Plan Assessment &amp; Design For</h4>
  				<img src="{{ $client_logo_url }}" width="200" class="mx-auto d-block" />
  				<h4>Presented By</h4>
          <img src="{{ Auth::user()->currentTeam->photo_url }}" width="150" class="mx-auto d-block"/>
        </div>
      </main>
    </div>
  </div>
  <div class="next-button">
    <a href="{{ $reasons_url }}"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
  </div>
@endsection
