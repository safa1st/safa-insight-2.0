@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('safa-content')
<script>
    var year_total = {{$year_end - $year_start}} + 1;
    var total_replacement_cost = 0;
    var avg_turnover_percent = 0;

    function addReplacementCost(replacement_cost)
    {
        total_replacement_cost += replacement_cost;
    }

    function addTurnoverPercent(turnover_percent)
    {
        avg_turnover_percent += turnover_percent;
    }
</script>
<div class="container-fluid">
    <h3>Turnover Analysis</h3>
    <table class="turnover-analysis-tab" style="padding-spacing: 0px; border-spacing: 0px; border-collapse: separate;">
        <tr style="height: 30px;">
            <td style="width: 160px; white-space: nowrap; text-align: center; border-bottom: 2px solid #E1E1E1;">
                    <a href="{{ $ta_summary_url }}" class="nav-link">Summary Report</a>
            </td>
            <td style="width: 160px; white-space: nowrap; text-align: center; border-top: 2px solid #E1E1E1; border-left: 2px solid #E1E1E1; border-right: 2px solid #E1E1E1">
                    <a href="{{ $ta_url }}" class="nav-link disabled" style="font-weight: 600;">Report By Year</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 10px; border-bottom: 2px solid #E1E1E1; border-left: 2px solid #E1E1E1; border-right: 2px solid #E1E1E1;">
                    
                    @if(count($year_list) > 0)
                    {{Form::open(array('method' => 'post', 'route' => ['insight.clientfocus.ta.report', $client_id], 'onsubmit' => 'return ValidateYearRange()'))}}
                    {{ Form::hidden('client_id', $client_id) }}
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>Start Year: </td>
                                        <td>{{Form::select('year_start', $year_list, $year_start, ['class' => 'form-control', 'id' => 'year_start'])}}</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>End Year: </td>
                                        <td>{{Form::select('year_end', $year_list, $year_end, ['class' => 'form-control', 'id' => 'year_end'])}}</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td><button type="submit" class="form-control">Go</button></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                @if($state == "DEFAULT")
                                    <h6>Reporting is based on our default benchmarking data.</h6>
                                @else
                                    @if(preg_match('/MSIE/i',$_SERVER['HTTP_USER_AGENT']) || preg_match('/Trident\/7.0; rv:11.0/', $_SERVER['HTTP_USER_AGENT']))
                                        <h6>Reporting is based on benchmarking data for the state of {{$state}}</h6>
                                    @else
                                        <img src="/{{$flag_path}}" width="75" style="border-style: solid; border-width: 1px; border-color: #C1C1C1" data-toggle="tooltip" title="Reporting is based on benchmarking data for the state of {{$state}}"/>
                                    @endif    
                                @endif
                            </td>
                        </tr>
                    </table>
                    {{ Form::close() }}
                    <table class="" style="width: 900px">
                        <tr>
                            <td class="align-top" style="width: 700px">
                                @foreach($year_dataset as $year)
                                <table class="no-padding" id="ta-year-container-{{$year}}">
                                    <thead class="w-100">
                                        <th colspan="5" class="w-100">
                                            <span class="panel-title">
                                                <br/>
                                            <a style="font-weight: 600; font-size: 12pt;" href="#" @if ($loop->first) class="expand-grp" @else class="collapsed expand-grp" @endif data-toggle="collapse" data-target="#ta-year-body-{{$year}}" aria-expanded="true" aria-controls="ta-year-body-{{$year}}">{{$year}}</a>
                                            </span>
                                        </th>
                                    </thead>
                    
                                    <tbody style="background-color: #F4F5F7;" id="ta-year-body-{{$year}}" data-parent="#ta-year-container-{{$year}}" @if ($loop->first) class="show" @else class="collapse" @endif>
                                            <tr style="height: 30px; background-color: #E1E1E1;">
                                                <td style="font-weight: 600; width: 120px; white-space: nowrap">
                                                    &nbsp;&nbsp;Employee Type
                                                </td>
                                                <td style="font-weight: 600; width: 160px; white-space: nowrap">
                                                    Avg Replacement Cost
                                                </td>
                                                <td style="font-weight: 600; width: 90px; white-space: nowrap">
                                                    Turnover
                                                </td>
                                                <td style="font-weight: 600; width: 120px; white-space: nowrap">
                                                    Your Workforce
                                                </td>
                                                <td style="font-weight: 600; width: 170px; white-space: nowrap">
                                                    Actual Replacement Cost
                                                </td>
                                            </tr>
                                        <!-- Overall -->
                                        @php ($row = $ta_dataset['overall'.$year])
                                        <tr style="height: 30px;">
                                            <td style="font-weight: 600; white-space: nowrap">
                                                &nbsp;&nbsp;{{ ucwords($row->job_category) }}
                                            </td>
                                            <td style="font-weight: 600;">
                                                ${{ number_format($row->avg_replacement_cost) }}
                                            </td>
                                            <td style="font-weight: 600; white-space: nowrap">
                                                {{ number_format($row->turnover_percent * 100, 2) }}%
                                                ({{ number_format($row->turnover_count) }})
                                                <script>addTurnoverPercent({{$row->turnover_percent}});</script>
                                            </td>
                                            <td style="font-weight: 600;">
                                                {{ number_format($row->employee_count) }}
                                            </td>
                                            <td style="font-weight: 600;">
                                                ${{ number_format($row->replacement_cost) }}
                                                <script>addReplacementCost({{$row->replacement_cost}});</script>
                                            </td>
                                        </tr>
                    
                                        <!-- Job Type Overall -->
                                        <tr style="height: 30px;">
                                            <td class="accordion-body" colspan="5">
                                                @foreach($job_type_dataset as $job_type)
                                                    @if(isset($ta_dataset[$job_type.$year]))
                                                        @php ($row = $ta_dataset[$job_type.$year])
                                                    @endif
                                                    @if(isset($row))
                                                        <table class="table-striped table-borderless no-padding" id="ta-jc-container-{{$year . $row->job_category}}">
                                                            <thead>
                                                                <th style="width: 120px; height: 30px; ">
                                                                    &nbsp;
                                                                    <span class="panel-title">
                                                                        <a class="collapsed expand-grp" data-toggle="collapse" data-parent="#ta-jc-container-{{$year . $row->job_category}}" href="#ta-jc-body-{{$year . $row->job_category}}" aria-expanded="false" aria-controls="ta-jc-body-{{$year . $row->job_category}}">
                                                                        {{ ucwords($row->job_category) }}
                                                                        </a>
                                                                    </span>
                                                                </th>
                                                                <th style="width: 160px">
                                                                    ${{ number_format($row->avg_replacement_cost) }}
                                                                </th>
                                                                <th style="width: 90px">
                                                                    {{ number_format($row->turnover_percent * 100, 2) }}%
                                                                    ({{ number_format($row->turnover_count) }})
                                                                </th>
                                                                <th style="width: 120px">
                                                                    {{ number_format($row->employee_count) }}
                                                                </th>
                                                                <th style="width: 170px">
                                                                    ${{ number_format($row->replacement_cost) }}
                                                                </th>
                                                            </thead>
                                                            <tbody class="accordion-body collapse" id="ta-jc-body-{{$year . $row->job_category}}">
                                                                @foreach($gen_dataset as $key => $value)
                    
                                                                    @if(isset($ta_dataset[$job_type.$key.$year]))
                                                                        @php ($row = $ta_dataset[$job_type.$key.$year])
                                                                    @else
                                                                        @php ($row = null)
                                                                    @endif
                                                                    @if($row)
                                                                    <tr style="height: 30px;">
                                                                        <td style="white-space: nowrap">
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ucwords($row->generation) }}
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td style="white-space: nowrap">
                                                                            {{ number_format($row->turnover_percent * 100, 2) }}%
                                                                            ({{ number_format($row->turnover_count) }})
                                                                        </td>
                                                                        <td>
                                                                            {{ number_format($row->employee_count) }}
                                                                        </td>
                                                                        <td>
                                                                            ${{ number_format($row->replacement_cost) }}
                                                                        </td>
                                                                    </tr>
                                                                    @else
                                                                    <tr style="height: 30px;">
                                                                        <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$gen_dataset[$key]}}</td><td>&nbsp;</td><td>-</td><td>-</td><td>-</td>
                                                                    </tr>
                                                                    @endif
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    @endif
                    
                                                @endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endforeach
                            </td>
                            <td valign="top">
                                @if(count($year_dataset) > 0)
                                <br/><br/>
                                <table>
                                    <tr>
                                        <td valign="middle">
                                            Total Replacement Cost:
                                            <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="This number represents the total cost of turnover for the date range selected ({{$year_start .' - '. $year_end}})"></i>
                                            <div id="rcs" style="font-size: 18pt; color: gray}}">
                                            </div>
                    
                                            <br/>
                                            Average Turnover %:
                                            <i class="fa fa-info-circle" style="font-size:16px;color:lightblue" data-toggle="tooltip" title="This number represents the average turnover percentage for the date range selected ({{$year_start .' - '. $year_end}})"></i>
                                            <div id="rrc" style="font-size: 18pt; color: gray}}">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                @endif
                            </td>
                        </tr>
                    </table>
                    </div>
                    @else
                        No Data Imported
                    @endif
            </td>
        </tr>
    </table>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


<div class="prev-button">
  <a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
</div>
<div class="next-button">
  <a href="td"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
</div>

@endsection

@push('css')
<style TYPE="text/css">
th {
    font-weight: 400;
}

.panel-title > a:before {
    font-family: FontAwesome;
    content:"\f068";
    padding-right: 5px;
}
.panel-title > a.collapsed:before {
    content:"\f067";
}
.panel-title > a:hover,
.panel-title > a:active,
.panel-title > a:focus  {
    text-decoration:none;
}

.expand-grp
{
    color: #424A55;
}

.no-padding
{
    padding: 0 !important;
    margin: 0 !important;
}

</style>
@endpush

@push('scripts')
<script>

function ValidateYearRange()
  {
      var startYear = $('#year_start').val();
      var endYear = $('#year_end').val();

      if (!startYear || !endYear)
      {
          alert("Invalid dates selected");
          return false;
      }

      try
      {
          if (endYear < startYear)
          {
              alert("Start Year cannot be later than End Year.");
              return false;
          }
      }
      catch(err)
      {
          alert("Invalid years selected");
          return false;
      }

      return true;
  }

    $(document).ready(function(){
        $('#rcs').html('$' + Number(total_replacement_cost).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

        $('#rrc').html(Number((avg_turnover_percent * 100) / year_total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '%');

        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endpush
