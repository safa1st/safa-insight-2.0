@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('safa-content')
<meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container-fluid">
    <div class="row">
          <div class="col-md-12">
            <h3>Trending Data</h3>
            <div class="card card-default chart-wrapper">
              <div class="card-header">
                <table width="100%">
                  <tr>
                    <td align="left">
                      Replacement Costs
                    </td>
                    <td align="right">
                      <table>
                          <tr>
                                <td>
                                    <select class="form-control" style="width: 200px; font-size: 10pt" id="rc_generation" onchange="getReplacementCostData()">
                                        <option value="all">Generation: All</option>
                                        <option value="genz">Generation: Gen Z</option>
                                        <option value="millennials">Generation: Millennials</option>
                                        <option value="genx">Generation: Gen X</option>
                                        <option value="babyboomer">Generation: Baby Boomer</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" style="width: 200px; font-size: 10pt" id="rc_jobtype" onchange="getReplacementCostData()">
                                        <option value="all">Job Type: All</option>
                                        <option value="staff">Job Type: Staff</option>
                                        <option value="management">Job Type: Management</option>
                                        <option value="executive">Job Type: Executive</option>
                                    </select>
                                </td>
                          </tr>
                      </table>
                    </td>
                    <td align="right">
                        @if($state == "DEFAULT")
                            <h6>Reporting is based on our default benchmarking data.</h6>
                        @else
                            <img src="/{{$flag_path}}" width="75" style="border-style: solid; border-width: 1px; border-color: #C1C1C1" data-toggle="tooltip" title="Reporting is based on benchmarking data for the state of {{$state}}"/>
                        @endif
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body">
                  <canvas id="rc_line" ></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-default chart-wrapper">
              <div class="card-header">
                <table width="100%">
                  <tr>
                    <td align="left">
                      Turnover Count
                    </td>
                    <td align="right">
                        <table>
                            <tr>
                                <td>
                                    <select class="form-control" style="width: 200px; font-size: 10pt" id="tc_generation" onchange="getTurnoverCountData()">
                                        <option value="all">Generation: All</option>
                                        <option value="genz">Generation: Gen Z</option>
                                        <option value="millennials">Generation: Millennials</option>
                                        <option value="genx">Generation: Gen X</option>
                                        <option value="babyboomer">Generation: Baby Boomer</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" style="width: 200px; font-size: 10pt" id="tc_jobtype" onchange="getTurnoverCountData()">
                                        <option value="all">Job Type: All</option>
                                        <option value="staff">Job Type: Staff</option>
                                        <option value="management">Job Type: Management</option>
                                        <option value="executive">Job Type: Executive</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body">
                <canvas id="tc_line"></canvas>
              </div>
            </div>
          </div>
    </div>
    <div class="prev-button">
      <a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
    </div>
    <div class="next-button">
      <a href="final"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
    </div>
  </div>


@endsection



@push('scripts')
<script type="text/javascript">
    function getReplacementCostData()
    {
        client_id = {{$client_id}};
        rc_generation = $( "#rc_generation" ).val();
        rc_jobtype = $( "#rc_jobtype" ).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/clients/'+client_id+'/td/rc',
            type: "POST",
            data: {
                _token : $('meta[name="csrf-token"]').attr('content'),
                client_id: client_id,
                rc_generation: rc_generation,
                rc_jobtype: rc_jobtype
            },
            dataType: "json",
            success:function(data) {
                var years = [];
                var replacement_costs = [];
                $(jQuery.parseJSON(JSON.stringify(data))).each(function() {
                        years.push(this.year);
                        replacement_costs.push(this.replacement_cost);
                });

                var chartData = {
                    labels: years,
                    datasets: [
                        {
                            label: "Replacement Costs",
                            fill: false,
                            lineTension: .1,
                            borderColor: '#ff6384',
                            data: replacement_costs
                        }
                    ]

                }

                new Chart(document.querySelector('#rc_line').getContext('2d') , {
                    type: "line",
                    data: chartData,
                    options : {
                        //animation: false,
                        legend: {display: false},
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Cost'
                                },
                                ticks: {
                                    beginAtZero: true,
                                    callback: function(value, index, values) {
                                        if(parseInt(value) >= 1000){
                                            return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else {
                                            return '$' + value;
                                        }
                                    }
                                }
                            }],
                            xAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Year'
                                }
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    if(parseInt(tooltipItem.yLabel) >= 1000){
                                        return '$' + tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return '$' + tooltipItem.yLabel;
                                    }
                                }
                            }
                        }
                    }
                });
            },
            error: function(data) {
                alert("An error has occurred receviing trending data");
            },
        });
    }

    function getTurnoverCountData()
    {
        client_id = {{$client_id}};
        tc_generation = $( "#tc_generation" ).val();
        tc_jobtype = $( "#tc_jobtype" ).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/clients/'+client_id+'/td/tc',
            type: "POST",
            data: {
                _token : $('meta[name="csrf-token"]').attr('content'),
                client_id: client_id,
                tc_generation: tc_generation,
                tc_jobtype: tc_jobtype
            },
            dataType: "json",
            success:function(data) {
                var years = [];
                var turnover_count = [];
                $(jQuery.parseJSON(JSON.stringify(data))).each(function() {
                        years.push(this.year);
                        turnover_count.push(this.turnover_count);
                });

                var chartData = {
                    labels: years,
                    datasets: [
                        {
                            label: "Turnover Count",
                            fill: false,
                            lineTension: .1,
                            borderColor: 'blue',
                            data: turnover_count
                        }
                    ]

                }

                new Chart(document.querySelector('#tc_line').getContext('2d') , {
                    type: "line",
                    data: chartData,
                    options : {
                        //animation: false,
                        legend: {display: false},
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Number of People'
                                }
                            }],
                            xAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Year'
                                }
                            }]
                        }
                    }
                });
            },
            error: function(data) {
                alert("An error has occurred receviing trending data");
            },
        });
    }

    $(document).ready(function() {
        getReplacementCostData({{$client_id}});
        getTurnoverCountData({{$client_id}});
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endpush
