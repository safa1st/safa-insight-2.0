@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('safa-content')
  <div class="container-fluid h-100">
    <div class="row justify-content-center">
  		<div class="col-sm-10">
        <h2 class="text-center" style='margin: 30px 0px 10px 0px'>Thank you for completing your assessment and design using</h2>
        <p class="text-center"><img style='margin: 100px 0px 30px 0px' src="{{asset('/img/safa_insight_original.png')}}" width="250" /></p>
        <h3 class="text-center">We're here to help</h3>
  		</div>
    </div>
    <div class="row justify-content-center">
  		<div class="col-sm-4" style="border-right: 1px solid gray;">
        <img src="{{ Auth::user()->currentTeam->photo_url }}" width="150" align="right" class="mx-auto d-block"/>
      </div>
      <div class="col-sm-2">
        <img src="{{ Auth::user()->photo_url }}" width="150" class="mx-auto d-block"/>
      </div>
      <div class="col-sm-4">
        <h3>{{ Auth::user()->name }}</h3>
        <h4>{{ Auth::user()->currentTeam->name }}</h4>
      </div>
    </div>
  </div>
  <div class="prev-button">
    <a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
  </div>
@endsection
