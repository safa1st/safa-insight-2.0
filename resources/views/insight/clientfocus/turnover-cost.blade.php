@extends($is_for_print ? 'insight.layouts.clientfocusprint' : 'insight.layouts.clientfocus')

@section('body_class', 'clientfocus clientfocus__turnover-cost')

@section('safa-content')
  <div class="container-fluid h-100">
    <div class="row h-100">
      <div class="col-lg-12">
        <h3>The Cost of Turnover</h3>
      </div>
  		<div class="col-sm-6 h-100">
  			<div>
  				<h4>Direct Cost</h4>
          <ul>
            <li>Recruitment costs</li>
            <li>Advertising costs for new position</li>
            <li>Orientation and training of new employee</li>
            <li>Severance costs</li>
            <li>Testing costs</li>
            <li>Time to interview new replacements</li>
            <li>Time to recruit and train new hires</li>
          </ul>
  			</div>
  		</div>
      <div class="col-sm-6 h-100">
  			<div>
  				<h4>Indirect Cost</h4>
          <ul>
            <li>Lost knowledge</li>
            <li>Loss of productivity while new employee is brought up to speed</li>
            <li>Reduced productivity of other employees</li>
            <li>Cost associated with lack of motivation prior to leaving</li>
            <li>Opportunity cost from being shorthanded</li>
            <li>Reduced morale</li>
            <li>Cost associated with loss of trade secrets</li>
          </ul>
  			</div>
  		</div>
    </div>
  </div>
  <div class="prev-button">
    <a href="{{ $previous_url }}"><button type="button" class="btn btn-primary btn-lg">Previous</button></a>
  </div>
  <div class="next-button">
    <a href="{{ $ta_url }}"><button type="button" class="btn btn-primary btn-lg">Next</button></a>
  </div>
@endsection
