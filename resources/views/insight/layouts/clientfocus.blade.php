@extends('spark::layouts.app')

@section('body_class', 'clientfocus')

@section('content')

<div class="wrapper">
  <div class="company-sidebar">
      @include('insight.nav.dashboard')
  </div>
  <div class="client-sidebar">
      @include('insight.nav.client')
  </div>
  
  <div class="client-content w-100">
    <div class="container-fluid">
      <div class="row d-flex">
        <div class="col-md-12">
          @yield('safa-content')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
