<html>
<head>

  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

  <script
  src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="/css/pdf.css" />


</head>

<body>
  <div class="page home">
    {!! $homeContent !!}
  </div>
  <div class="page">
    {!! $reasonsContent !!}
  </div>
  <div class="page turnover-cost">
    {!! $turnoverCostContent !!}
  </div>
  <div class="page">
    {!! $turnoverAnalysisContent !!}
    </div><!--Fixing closing div from TA-->
  </div>
  <div class="page">
    {!! $trendingDataContent !!}
  </div>
  <div class="page">
    {!! $finalContent !!}
  </div>
</body>
</html>
