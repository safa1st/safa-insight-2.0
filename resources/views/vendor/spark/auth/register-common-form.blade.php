<form role="form">
    @if (Spark::usesTeams() && Spark::onlyTeamPlans())
        <!-- Team Name -->
        <div class="form-group row" v-if=" ! invitation">
            <label class="col-md-4 col-form-label text-md-right">{{ __('teams.team_name') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="team" v-model="registerForm.team" :class="{'is-invalid': registerForm.errors.has('team')}" autofocus>

                <span class="invalid-feedback" v-show="registerForm.errors.has('team')">
                    @{{ registerForm.errors.get('team') }}
                </span>
            </div>
        </div>

        @if (Spark::teamsIdentifiedByPath())
            <!-- Team Slug (Only Shown When Using Paths For Teams) -->
            <div class="form-group row" v-if=" ! invitation">
                <label class="col-md-4 col-form-label text-md-right">{{ __('teams.team_slug') }}</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="team_slug" v-model="registerForm.team_slug" :class="{'is-invalid': registerForm.errors.has('team_slug')}" autofocus>

                    <small class="form-text text-muted" v-show="! registerForm.errors.has('team_slug')">
                        {{__('teams.slug_input_explanation')}}
                    </small>

                    <span class="invalid-feedback" v-show="registerForm.errors.has('team_slug')">
                        @{{ registerForm.errors.get('team_slug') }}
                    </span>
                </div>
            </div>
        @endif
    @endif

    <!-- Name -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">{{__('Name')}}</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="name" v-model="registerForm.name" :class="{'is-invalid': registerForm.errors.has('name')}" autofocus>

            <span class="invalid-feedback" v-show="registerForm.errors.has('name')">
                @{{ registerForm.errors.get('name') }}
            </span>
        </div>
    </div>

    <!-- E-Mail Address -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">{{__('E-Mail Address')}}</label>

        <div class="col-md-6">
            <input type="email" style="text-transform:lowercase" onkeyup="this.value = this.value.toLowerCase();" class="form-control" name="email" v-model="registerForm.email" :class="{'is-invalid': registerForm.errors.has('email')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('email')">
                @{{ registerForm.errors.get('email') }}
            </span>
        </div>
    </div>

    <!-- Password -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">{{__('Password')}}</label>

        <div class="col-md-6">
            <input type="password" class="form-control" name="password" v-model="registerForm.password" :class="{'is-invalid': registerForm.errors.has('password')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('password')">
                @{{ registerForm.errors.get('password') }}
            </span>
        </div>
    </div>

    <!-- Password Confirmation -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">{{__('Confirm Password')}}</label>

        <div class="col-md-6">
            <input type="password" class="form-control" name="password_confirmation" v-model="registerForm.password_confirmation" :class="{'is-invalid': registerForm.errors.has('password_confirmation')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('password_confirmation')">
                @{{ registerForm.errors.get('password_confirmation') }}
            </span>
        </div>
    </div>

    <!-- Address -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">Address</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="address"
                v-model="registerForm.address"
                :class="{'is-invalid': registerForm.errors.has('address')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('address')">
                @{{ registerForm.errors.get('address') }}
            </span>
        </div>
    </div>

    <!-- City -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">City</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="city"
                v-model="registerForm.city"
                :class="{'is-invalid': registerForm.errors.has('city')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('city')">
                @{{ registerForm.errors.get('city') }}
            </span>
        </div>
    </div>

    <!-- State -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">State</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="state"
                v-model="registerForm.state"
                :class="{'is-invalid': registerForm.errors.has('state')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('state')">
                @{{ registerForm.errors.get('state') }}
            </span>
        </div>
    </div>

    <!-- Zip Code -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">Zip Code</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="zip"
                v-model="registerForm.zip"
                :class="{'is-invalid': registerForm.errors.has('zip')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('zip')">
                @{{ registerForm.errors.get('zip') }}
            </span>
        </div>
    </div>

    <!-- Country -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">Country</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="country"
                v-model="registerForm.country"
                :class="{'is-invalid': registerForm.errors.has('country')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('country')">
                @{{ registerForm.errors.get('country') }}
            </span>
        </div>
    </div>

    <!-- Business Phone -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">Business Phone</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="businessphone"
                v-model="registerForm.businessphone"
                :class="{'is-invalid': registerForm.errors.has('businessphone')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('businessphone')">
                @{{ registerForm.errors.get('businessphone') }}
            </span>
        </div>
    </div>

    <!-- Mobile Phone -->
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">Mobile Phone</label>

        <div class="col-md-6">
            <input type="text" class="form-control" name="mobilephone"
                v-model="registerForm.mobilephone"
                :class="{'is-invalid': registerForm.errors.has('mobilephone')}">

            <span class="invalid-feedback" v-show="registerForm.errors.has('mobilephone')">
                @{{ registerForm.errors.get('mobilephone') }}
            </span>
        </div>
    </div>

    <!-- Terms And Conditions -->
    <div v-if=" ! selectedPlan || selectedPlan.price == 0">
        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="terms" :class="{'is-invalid': registerForm.errors.has('terms')}" v-model="registerForm.terms">
                    <label class="form-check-label" for="terms">
                        {!! __('I Accept :linkOpen The Terms Of Service :linkClose', ['linkOpen' => '<a href="/terms" target="_blank">', 'linkClose' => '</a>']) !!}
                    </label>
                    <div class="invalid-feedback" v-show="registerForm.errors.has('terms')">
                        <strong>@{{ registerForm.errors.get('terms') }}</strong>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button class="btn btn-primary" @click.prevent="register" :disabled="registerForm.busy">
                    <span v-if="registerForm.busy">
                        <i class="fa fa-btn fa-spinner fa-spin"></i> {{__('Registering')}}
                    </span>

                    <span v-else>
                        <i class="fa fa-btn fa-check-circle"></i> {{__('Register')}}
                    </span>
                </button>
            </div>
        </div>
    </div>
</form>
