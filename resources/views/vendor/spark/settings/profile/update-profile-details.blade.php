<update-profile-details :user="user" inline-template>
    <div class="card">
        <div class="card-header">Profile Details</div>

        <div class="card-body">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your profile has been updated!
            </div>

            <form role="form">
                <!-- Address -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Address</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="address"
                               v-model="form.address"
                               :class="{'is-invalid': form.errors.has('address')}">

                        <span class="invalid-feedback" v-show="form.errors.has('address')">
                            @{{ form.errors.get('address') }}
                        </span>
                    </div>
                </div>

                <!-- City -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">City</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="city"
                            v-model="form.city"
                            :class="{'is-invalid': form.errors.has('city')}">

                        <span class="invalid-feedback" v-show="form.errors.has('city')">
                            @{{ form.errors.get('city') }}
                        </span>
                    </div>
                </div>

                <!-- State -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">State</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="state"
                            v-model="form.state"
                            :class="{'is-invalid': form.errors.has('state')}">

                        <span class="invalid-feedback" v-show="form.errors.has('state')">
                            @{{ form.errors.get('state') }}
                        </span>
                    </div>
                </div>

                <!-- Zip Code -->
                <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Zip Code</label>
    
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="zip"
                                v-model="form.zip"
                                :class="{'is-invalid': form.errors.has('zip')}">
    
                            <span class="invalid-feedback" v-show="form.errors.has('zip')">
                                @{{ form.errors.get('zip') }}
                            </span>
                        </div>
                    </div>
    

                <!-- Country -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Country</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="country"
                            v-model="form.country"
                            :class="{'is-invalid': form.errors.has('country')}">

                        <span class="invalid-feedback" v-show="form.errors.has('country')">
                            @{{ form.errors.get('country') }}
                        </span>
                    </div>
                </div>

                <!-- Business Phone -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Business Phone</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="businessphone"
                            v-model="form.businessphone"
                            :class="{'is-invalid': form.errors.has('businessphone')}">

                        <span class="invalid-feedback" v-show="form.errors.has('businessphone')">
                            @{{ form.errors.get('businessphone') }}
                        </span>
                    </div>
                </div>

                <!-- Mobile Phone -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Mobile Phone</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="mobilephone"
                            v-model="form.mobilephone"
                            :class="{'is-invalid': form.errors.has('mobilephone')}">

                        <span class="invalid-feedback" v-show="form.errors.has('mobilephone')">
                            @{{ form.errors.get('mobilephone') }}
                        </span>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="update"
                                :disabled="form.busy">

                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</update-profile-details>