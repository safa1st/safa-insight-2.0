
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

require('./components/bootstrap');

Spark.forms.register = {
    address: '',
    city: '',
    state: '',
    zip: '',
    country: '',
    businessphone: '',
    mobilephone: ''
};

Vue.component('select-client-nav-item', require('./components/clients/sidebar/select-client-nav-item.vue'));
Vue.component('view-edit-client', require('./components/clients/view-edit-client.vue'));
Vue.component('add-client', require('./components/clients/add-client.vue'));
Vue.component('delete-client', require('./components/clients/delete-client.vue'));
Vue.component('select-client-modal', require('./components/clients/select-client-modal.vue'));


var app = new Vue({
    mixins: [require('spark')]
});
