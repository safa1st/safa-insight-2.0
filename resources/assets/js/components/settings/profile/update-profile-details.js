Vue.component('update-profile-details', {
    props: ['user'],

    data() {
        return {
            form: new SparkForm({
                address: '',
                city: '',
                state: '',
                zip: '',
                country: '',
                businessphone: '',
                mobilephone: ''
            })
        };
    },

    mounted() {
        this.form.address = this.user.address;
        this.form.city = this.user.city;
        this.form.state = this.user.state;
        this.form.zip = this.user.zip;
        this.form.country = this.user.country;
        this.form.businessphone = this.user.businessphone;
        this.form.mobilephone = this.user.mobilephone;
    },

    methods: {
        update() {
            Spark.put('/settings/profile/details', this.form)
                .then(response => {
                    Bus.$emit('updateUser');
                });
        }
    }
});
