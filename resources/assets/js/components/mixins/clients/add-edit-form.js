export const addEditClientFormData = {
  data() {
    return {
      form: new SparkForm({
          name: '',
          industry: '',
          size: '',
          address: '',
          address2: '',
          city: '',
          state: '',
          zipcode: '',
          contact: {
            name: '',
            email: '',
            workPhone: '',
            mobilePhone: '',
          }
      }),
      logoFormData: null,
      isBusy: false,
    }
  },
  methods: {
    populateFormData(result){
      this.form.name = result.name;
      this.form.industry = result.industry;
      this.form.size = result.size;
      this.form.address = result.address;
      this.form.address2 = result.address2;
      this.form.city = result.city;
      this.form.state = result.state;
      this.form.zipcode = result.zipcode;
      this.form.contact.name = result.contact.name;
      this.form.contact.email = result.contact.email;
      this.form.contact.workPhone = result.contact.workPhone;
      this.form.contact.mobilePhone = result.contact.mobilePhone;
    }
  }
}
