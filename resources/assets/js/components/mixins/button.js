export const button = {
  props: {
    displayText: {
      type: String,
      required: true,
      default: '',
    },
    clickEventValue: {
      type: String,
      required: false,
      default: '',
    }
  },
  methods: {
    handleClick(event){
      event.preventDefault();
      this.$emit('clicked', this.clickEventValue);
    },
  }
}
