
const BASE_URL = '/feeds/default/private/clients';

function doCreateOrUpdate(fn, logoFormData, failureMessage){
  return fn()
    .then((client) => {
      if (logoFormData !== null){
        return axios.post(client.logo, logoFormData, {
            headers: { 'Content-Type': 'multipart/form-data'}
          }).then((uploadResponse) => {
            return Promise.resolve(client);
          });
      }
      else{
        return Promise.resolve(client);
      }
    });
};

export const clients = {
  fetchAll(){
    const promise = new Promise((resolve, reject) => {
      axios.get(BASE_URL).then((response) => {
        resolve(response.data);
      }, (reason) => {
        reject('Fetch failed');
      });
    });
    promise.then((value) => {
      return value;
    });

    return promise;
  },
  create(form, logoFormData){
    return doCreateOrUpdate(Spark.post.bind(null, BASE_URL, form),
      logoFormData,
      'Create Client Failed',
    );
  },
  update(url, form, logoFormData){
    return doCreateOrUpdate(Spark.put.bind(null, url, form),
      logoFormData,
      'Update Client Failed',);
  },
  delete(url){
    return axios.delete(url);
  }
}
