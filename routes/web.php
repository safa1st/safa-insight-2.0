<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@showAllClients');

Route::get('/home/clients', 'HomeController@showCreateClient')->name('insight.create.client');;

Route::prefix('/home/clients/{client_id}')
    ->namespace('Insight')
    ->middleware('auth')
    ->name('insight.dashboard')
    ->group(function () {
        Route::get('/edit', 'ClientDashboardController@edit')->name('.edit');
        // Workforce Profile Routes
        Route::get('/wp', 'WPController@index')->name('.wp.home');
        Route::get('/wp/{batch_id}', 'WPController@list')->name('.wp.list');
        Route::post('/wp', 'WPController@import')->name('.wp.upload');
        Route::post('/wp/delete', 'WPController@destroy')->name('.wp.delete');
    });

// Client Routes
Route::get('/feeds/default/private/clients', 'ClientsController@index');
Route::post('/feeds/default/private/clients', 'ClientsController@store');
Route::put('/feeds/default/private/clients/{id}', 'ClientsController@update')->name('client.update');
Route::delete('/feeds/default/private/clients/{id}', 'ClientsController@destroy')->name('client.delete');
Route::get('/feeds/default/private/clients/{id}/logo', 'ClientsController@indexlogo')->name('client.get.logo');
Route::post('/feeds/default/private/clients/{id}/logo', 'ClientsController@storelogo')->name('client.post.logo');

Route::put('/settings/profile/details', 'ProfileDetailsController@update');

Route::prefix('/clients/{client_id}')
    ->namespace('Insight')
    ->middleware('auth')
    ->name('insight.clientfocus.')
    ->group(function () {
        Route::get('/', 'ClientFocusController@show')->name('home');
        Route::get('/reasons', 'ClientFocusController@showReasons')->name('reasons');
        Route::get('/final', 'ClientFocusController@showFinal')->name('final');
        Route::get('/turnover-cost', 'ClientFocusController@showTurnoverCost')->name('turnover-cost');
        // Turnover Analysis Routes
        Route::get('/ta', 'TAController@index')->name('ta.home');
        Route::get('/ta/summary', 'TAController@index_summary')->name('ta.home.summary');
        Route::post('/ta', 'TAController@show')->name('ta.report');
        Route::post('/ta/summary', 'TAController@show_summary')->name('ta.report.summary');

        // Trending Data Routes
        Route::get('/td', 'TDController@index')->name('td.home');
        Route::post('/td/rc', 'TDController@td_replacement_cost')->name('td.rc');
        Route::post('/td/tc', 'TDController@td_turnover_count')->name('td.tc');

        // HTML for pdf creation
        Route::get('/allslides', 'ClientFocusController@showAllSides')->name('allslides');

        // HTML for pdf creation
        Route::get('/pdf', 'ClientFocusController@showPdf')->name('pdf');


    });

// [DISABLE REGISTRATION FOR PILOT]
Route::get( '/register', function() {
    return redirect()->intended( '/' );
});
