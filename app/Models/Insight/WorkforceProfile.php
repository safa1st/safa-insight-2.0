<?php

    namespace App\Models\Insight;

    use Illuminate\Database\Eloquent\Model;

    /**
     * @property mixed id
     */
    class WorkforceProfile extends Model
    {
        protected $fillable = array(
            'user_id', 
            'client_id', 
            'batch_id', 
            'identifier', 
            'first_name', 
            'last_name', 
            'dob', 
            'address1', 
            'address2', 
            'city', 
            'state', 
            'zip', 
            'country', 
            'email', 
            'phone', 
            'mobile', 
            'job_category', 
            'job_title', 
            'annual_salary', 
            'start_date', 
            'end_date', 
            'associations', 
            'deleted_flag'
        );
        protected $table = 'workforce_profile';

        public function user()
        {
            return $this->hasOne('App\User', 'id', 'user_id');
        }

        public function client()
        {
            return $this->hasOne('App\Client', 'id', 'client_id');
        }

        public function batch()
        {
            return $this->hasOne('App\Models\Insight\Batch', 'id', 'batch_id');
        }
    }
?>