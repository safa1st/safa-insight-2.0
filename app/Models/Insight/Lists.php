<?php

    namespace App\Models\Insight;

    use Illuminate\Database\Eloquent\Model;

    class Lists extends Model
    {
        protected $fillable = array(
            'type',
            'code',
            'name'
        );
        protected $table = 'lists';

        // File Formats
        const batch_type_WP = "WP";
        const batch_type_WPU = "WPU";

        // Person Types
        const job_category_E = "E";
        const job_category_M = "M";
        const job_category_S = "S";

    }
?>