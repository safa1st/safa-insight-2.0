<?php

    namespace App\Models\Insight;

    use Illuminate\Database\Eloquent\Model;

    /**
     * @property mixed id
     */
    class Batch extends Model
    {
        protected $fillable = array('user_id', 'client_id', 'title', 'file', 'batch_type_lid', 'deleted_flag');
        protected $table = 'batch';
        protected $casts = [
            'file' => 'array'
        ];

        public function user()
        {
            return $this->hasOne('App\User', 'id', 'user_id');
        }
    }
?>