<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use Carbon\Carbon;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Your Company',
        'product' => 'Your Product',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = "support@safa.io";

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'admin@safa.io'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = false;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        //Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::freePlan()
            ->features([
                'User Management', 'Client Management'
            ]);

        /*Spark::plan('Pilot', 'provider-id-1')
            ->price(50)
            ->features([
                'Turnover Analysis'
            ]);
        */


        // Use these roles for now. Names can be changed, but don't change keys
       Spark::useRoles([
            'advisor' => 'Advisor',
            'broker' => 'Broker'
        ]);

        // Teams need to be created during implementation, so diable team create
        Spark::noAdditionalTeams();

        Spark::validateUsersWith(function () {
            return [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
                'address' => 'max:255',
                'city' => 'max:255',
                'state' => 'max:255',
                'zip' => 'max:255',
                'country' => 'max:255',
                'businessphone' => 'max:255',
                'mobilephone' => 'max:255',
                'terms' => 'required|accepted',
            ];
        });

        Spark::createUsersWith(function ($request) {
            $user = Spark::user();
        
            $data = $request->all();
        
            $user->forceFill([
                'name' => $data['name'],
                'email' => $data['email'],
                'address' => $data['address'],
                'city' => $data['city'],
                'state' => $data['state'],
                'zip' => $data['zip'],
                'country' => $data['country'],
                'businessphone' => $data['businessphone'],
                'mobilephone' => $data['mobilephone'],
                'password' => bcrypt($data['password']),
                'last_read_announcements_at' => Carbon::now(),
                'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
            ])->save();
        
            return $user;
        });

    }
}
