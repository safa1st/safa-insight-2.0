<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Route;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Resource::withoutWrapping();

        if(env('APP_ENV') === 'production') {
            URL::forceScheme('https');
        }

        view()->composer('insight.nav.client', function ($view) {

          $client_id = Route::current()->parameter('client_id');

          $present_url = route('insight.clientfocus.home', ['client_id' => $client_id]);
          $logo_url = route('client.get.logo', ['id' => $client_id]);
          $wp_url = route('insight.dashboard.wp.home', ['client_id' => $client_id]);
          $edit_url = route('insight.dashboard.edit', ['client_id' => $client_id]);
          $print_url = route('insight.clientfocus.pdf', ['client_id' => $client_id]);

          $view->with(compact(
            'present_url',
            'edit_url',
            'logo_url',
            'wp_url',
            'print_url',
            'client'));
        });

        view()->composer('insight.nav.dashboard', function ($view) {
          $client_dashboard_url = '/home/clients';
          $view->with(compact(
            'client_dashboard_url'
          ));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
