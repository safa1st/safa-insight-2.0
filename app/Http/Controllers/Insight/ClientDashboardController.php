<?php

namespace App\Http\Controllers\Insight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client as ClientResource;
use App\Client;
use Illuminate\Support\Facades\Auth;

class ClientDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function edit($client_id)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      if (!$client){
        abort(404);
      }

      $client_resource = new ClientResource($client);
      $client_delete_url = route('client.delete', ['client_id' => $client_id]);


      return view('insight.dashboard.client.edit', [
        'client' => $client,
        'client_resource' => $client_resource,
        'client_delete_url' => $client_delete_url,
      ]);
    }
}
