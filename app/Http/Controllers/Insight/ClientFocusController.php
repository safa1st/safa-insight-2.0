<?php

namespace App\Http\Controllers\Insight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;

class ClientFocusController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show($client_id, $is_for_print = false)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      if (!$client){
        abort(404);
      }

      $client_logo_url = route('client.get.logo', ['id' => $client_id]);

      $reasons_url = route('insight.clientfocus.reasons', ['client_id' => $client_id]);

      return view('insight.clientfocus.home', [
        'name' => $client->name,
        'is_for_print' => $is_for_print,
        'client_logo_url' => $client_logo_url,
        'reasons_url' => $reasons_url,
      ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showReasons($client_id, $is_for_print = false)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      $previous_url = route('insight.clientfocus.home', ['client_id' => $client_id]);
      $cost_url = route('insight.clientfocus.turnover-cost', ['id' => $client_id]);
      return view('insight.clientfocus.reasons', [
        'previous_url' => $previous_url,
        'cost_url' => $cost_url,
        'is_for_print' => $is_for_print,
      ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showTurnoverCost($client_id, $is_for_print = false)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      $previous_url = route('insight.clientfocus.reasons', ['client_id' => $client_id]);

      $ta_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);
      return view('insight.clientfocus.turnover-cost', [
        'previous_url' => $previous_url,
        'ta_url' => $ta_url,
        'is_for_print' => $is_for_print,
      ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showFinal($client_id, $is_for_print = false)
    {
      $previous_url = route('insight.clientfocus.td.home', ['client_id' => $client_id]);
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();
      return view('insight.clientfocus.final', [
        'previous_url' => $previous_url,
        'is_for_print' => $is_for_print
      ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showAllSides($client_id)
    {
      $homeContent = $this->show($client_id, true)->render();
      $reasonsContent = $this->showReasons($client_id, true)->render();
      $turnoverCostContent = $this->showTurnoverCost($client_id, true)->render();

      $turnoverAnalysisContent = app('App\Http\Controllers\Insight\TAController')->index($client_id, true)->render();
      $trendingDataContent = app('App\Http\Controllers\Insight\TDController')->index($client_id, true)->render();

      $finalContent = $this->showFinal($client_id, true)->render();

      return view('insight.layouts.clientfocusprintallpages', [
        'homeContent' => $homeContent,
        'reasonsContent' => $reasonsContent,
        'turnoverCostContent' => $turnoverCostContent,
        'turnoverAnalysisContent' => $turnoverAnalysisContent,
        'trendingDataContent' => $trendingDataContent,
        'finalContent' => $finalContent,
      ]);
    }

    /**
     *
     * @return Response
     */
    public function showPdf($client_id)
    {

      $all_slides_url = route('insight.clientfocus.allslides', ['client_id' => $client_id]);

      $client = new GuzzleClient();
      $req = $client->request('GET', env('PDF_GENERATOR_URL'),
        [
          'query' => [
            'url' => $all_slides_url,
            'cookieName' => 'safa_insight_session',
            'cookieValue' => $_COOKIE["safa_insight_session"],
          ]
        ]
      );

      $response = response()->make($req->getBody()->getContents(), 200);
      $response->header('Content-Type', 'application/pdf');
      return $response;

    }
}
