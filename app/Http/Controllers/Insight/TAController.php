<?php

namespace App\Http\Controllers\Insight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class TAController extends Controller
{
    // Member Variables
    private $mUserId;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id, $is_for_print = false)
    {
        $this->mUserId = auth()->id();

        $year_list = $this->getYearList($client_id);
        $previous_url = route('insight.clientfocus.turnover-cost', ['id' => $client_id]);

        if (count($year_list) > 0)
        {
            $year_end = array_values($year_list)[0];
            return $this->showTA($client_id, $year_end - 1, $year_end, $previous_url, $is_for_print);
        }
        else
        {
            $ta_dataset = array();
            $year_dataset = array();
            $job_type_dataset = array();
            $gen_dataset = array();
            $year_list = $this->getYearList($client_id);

            $year_start = date("Y") - 1;
            $year_end = date("Y");

            $ta_summary_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);
            $ta_url = route('insight.clientfocus.ta.home', ['id' => $client_id]);

            $state = "DEFAULT";
            $flag_path = "";
            $this->getReportingLocation($client_id, $state, $flag_path);

            return view(
                'insight.clientfocus.ta-report',
                compact(
                    'client_id',
                    'ta_dataset',
                    'year_dataset',
                    'job_type_dataset',
                    'gen_dataset',
                    'year_list',
                    'year_start',
                    'year_end',
                    'previous_url',
                    'is_for_print',
                    'ta_url',
                    'ta_summary_url',
                    'state',
                    'flag_path')
                );
        }
    }

    public function index_summary($client_id)
    {
        $this->mUserId = auth()->id();

        $year_list = $this->getYearList($client_id);
        $previous_url = route('insight.clientfocus.turnover-cost', ['id' => $client_id]);

        if (count($year_list) > 0)
        {
            $start_date = "";
            $end_date = "";
            $this->getYearRange($client_id, $start_date, $end_date);
            return $this->showTASummary($client_id, $start_date, $end_date, $previous_url);
        }
        else
        {
            $ta_dataset = array();
            $job_type_dataset = array();
            $gen_dataset = array();

            $ta_summary_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);
            $ta_url = route('insight.clientfocus.ta.home', ['id' => $client_id]);

            $state = "DEFAULT";
            $flag_path = "";
            $this->getReportingLocation($client_id, $state, $flag_path);

            return view(
                'insight.clientfocus.ta-report-summary',
                compact(
                    'client_id',
                    'ta_dataset',
                    'start_date',
                    'end_date',
                    'job_type_dataset',
                    'gen_dataset',
                    'previous_url',
                    'ta_url',
                    'ta_summary_url',
                    'state',
                    'flag_path')
                );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->mUserId = auth()->id();

        $client_id = $request->get('client_id');
        $year_start = $request->get('year_start');
        $year_end = $request->get('year_end');
        $previous_url = route('insight.clientfocus.turnover-cost', ['id' => $client_id]);

        return $this->showTA($client_id, $year_start, $year_end, $previous_url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_summary(Request $request)
    {
        $this->mUserId = auth()->id();

        $client_id = $request->get('client_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $previous_url = route('insight.clientfocus.turnover-cost', ['id' => $client_id]);

        return $this->showTASummary($client_id, $start_date, $end_date, $previous_url);
    }

    /// Private Methods
    private function getReportingLocation($client_id, &$state, &$flag_path)
    {
        $dataset = DB::select("
            WITH supported_states AS (
                SELECT DISTINCT state_code FROM turnover_percent
            ),

            reporting_state AS (
                SELECT 
                CASE
                    WHEN (state IN (SELECT state_code from supported_states)) THEN state
                    ELSE 'DEFAULT'
                END AS state
                FROM clients
                WHERE user_id = ? and id = ?
            )

            SELECT l.state_name, l.flag_path
            FROM reporting_state rs
            JOIN location l ON l.state_code = rs.state
            ",
            [$this->mUserId, $client_id]
        );

        $state = "DEFAULT";
        $flag_path = "";
        // Will only be one row
        foreach($dataset as $row)
        {
            $state = $row->state_name;
            $flag_path = $row->flag_path;
        }
    }

    private function showTA($client_id, $year_start, $year_end, $previous_url, $is_for_print = false)
    {
        $this->mUserId = auth()->id();

        $ta_dataset = array();
        $year_dataset = array();

        $job_type_dataset = array();
        $job_type_dataset['executive'] = 'executive';
        $job_type_dataset['management'] = 'management';
        $job_type_dataset['staff'] = 'staff';

        $gen_dataset = array();
        $gen_dataset['babyboomer'] = 'Baby Boomer';
        $gen_dataset['genx'] = 'Gen X';
        $gen_dataset['millennials'] = 'Millennials';
        $gen_dataset['genz'] = 'Gen Z';

        $ta_dataset = $this->getTaReport($client_id, $year_start, $year_end, $year_dataset);

        $year_list = $this->getYearList($client_id);

        $ta_summary_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);
        $ta_url = route('insight.clientfocus.ta.home', ['id' => $client_id]);

        $state = "DEFAULT";
        $flag_path = "";
        $this->getReportingLocation($client_id, $state, $flag_path);

        return view(
            'insight.clientfocus.ta-report',
            compact(
                'client_id',
                'ta_dataset',
                'year_dataset',
                'job_type_dataset',
                'gen_dataset',
                'year_list',
                'year_start',
                'year_end',
                'previous_url',
                'ta_url',
                'ta_summary_url',
                'is_for_print',
                'state',
                'flag_path')
            );
    }

    private function showTASummary($client_id, $start_date, $end_date, $previous_url)
    {
        $this->mUserId = auth()->id();

        $ta_dataset = array();
        $year_dataset = array();

        $job_type_dataset = array();
        $job_type_dataset['executive'] = 'executive';
        $job_type_dataset['management'] = 'management';
        $job_type_dataset['staff'] = 'staff';

        $gen_dataset = array();
        $gen_dataset['babyboomer'] = 'Baby Boomer';
        $gen_dataset['genx'] = 'Gen X';
        $gen_dataset['millennials'] = 'Millennials';
        $gen_dataset['genz'] = 'Gen Z';

        $ta_dataset = $this->getTaReportRange($client_id, $start_date, $end_date);

        $ta_summary_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);
        $ta_url = route('insight.clientfocus.ta.home', ['id' => $client_id]);

        $state = "DEFAULT";
        $flag_path = "";
        $this->getReportingLocation($client_id, $state, $flag_path);

        return view(
            'insight.clientfocus.ta-report-summary',
            compact(
                'client_id',
                'ta_dataset',
                'start_date',
                'end_date',
                'job_type_dataset',
                'gen_dataset',
                'previous_url',
                'ta_url',
                'ta_summary_url',
                'state',
                'flag_path')
            );
    }

    private function getYearList($client_id)
    {
        $dataset = DB::select("SELECT
            MIN(EXTRACT(YEAR FROM start_date)) AS year_start,
            CASE
                WHEN MAX(EXTRACT(YEAR FROM end_date)) IS NULL THEN MAX(EXTRACT(YEAR FROM start_date))
                ELSE MAX(EXTRACT(YEAR FROM end_date))
            END AS year_end
            FROM workforce_profile
            WHERE user_id = ? AND client_id = ?

                ", [$this->mUserId, $client_id]
        );

        $year_list = array();
        foreach($dataset as $row)
        {
            if ($row->year_start)
            {
                for ($year = $row->year_end; $year >= $row->year_start; $year--)
                {
                    $year_list[$year] = $year ;
                }
            }
        }
        return $year_list;
    }

    private function getYearRange($client_id, &$start_date, &$end_date)
    {
        $dataset = DB::select("SELECT
            to_char(MIN(start_date), 'MM/DD/YYYY') AS start_date,
            CASE
                WHEN MAX(end_date) IS NULL THEN to_char(MAX(start_date), 'MM/DD/YYYY')
                ELSE to_char(MAX(end_date), 'MM/DD/YYYY')
            END AS end_date
            FROM workforce_profile
            WHERE user_id = ? AND client_id = ?

                ", [$this->mUserId, $client_id]
        );

        foreach($dataset as $row)
        {
            $start_date = $row->start_date;
            $end_date = $row->end_date;
        }
    }

    private function getTaReport($client_id, $year_start, $year_end, &$year_dataset)
    {
        for ($year = $year_end; $year >= $year_start; $year--)
        {
            $year_dataset[$year] = $year;

            // Done
            $dataset = DB::select("WITH turnover_percent AS (
                    SELECT job_category, generation, replacement_cost, state_code
                    FROM turnover_percent
                ),
                supported_states AS (
                    SELECT DISTINCT state_code
                    FROM turnover_percent
                ),
                employee_type_calc AS (
                    SELECT DISTINCT ON (wp.identifier)
                    wp.id,
                        wp.job_category,
                        CASE
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                            ELSE 'Other'
                        END AS generation,
                        wp.annual_salary,
                        CASE
                            WHEN (wp.end_date IS NOT NULL AND EXTRACT(YEAR FROM wp.end_date) = ?) THEN 1
                            ELSE 0
                        END AS turnover_flag,
                        CASE
                            WHEN (cl.state IN (SELECT state_code from supported_states)) THEN cl.state
                            ELSE 'DEFAULT'
                        END AS state
                    FROM workforce_profile wp
                    JOIN clients cl on wp.client_id = cl.id
                    WHERE wp.user_id = ? AND wp.client_id = ?
                    AND (wp.end_date IS NULL OR EXTRACT(YEAR FROM wp.end_date) >= ?)
                    AND EXTRACT(YEAR FROM wp.start_date) <= ?
                    GROUP BY wp.id, wp.job_category, generation, cl.state
                    ORDER BY wp.identifier, wp.created_at DESC
                )

                SELECT
                    replace(lower(concat(etc.job_category, etc.generation)),' ','') as key,
                    etc.job_category,
                    etc.generation,
                    AVG(etc.annual_salary * tp.replacement_cost) AS avg_replacement_cost,
                    CAST(SUM(etc.turnover_flag) AS decimal) / CAST(COUNT(*) AS DECIMAL) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                GROUP BY etc.job_category, etc.generation
                UNION
                SELECT
                    'overall' as key,
                    'overall' as job_category,
                    'all' as generation,
                    cast(SUM(etc.annual_salary * tp.replacement_cost) AS decimal) / cast(COUNT(*) AS decimal) AS avg_replacement_cost,
                    cast(SUM(etc.turnover_flag) AS decimal) / cast(COUNT(*) AS decimal) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                UNION
                SELECT
                    etc.job_category as key,
                    etc.job_category as job_category,
                    '' as generation,
                    cast(SUM(etc.annual_salary * tp.replacement_cost) AS decimal) / cast(COUNT(*) AS decimal) AS avg_replacement_cost,
                    cast(SUM(etc.turnover_flag) AS decimal) / cast(COUNT(*) AS decimal) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                GROUP BY etc.job_category
                ",
                [$year, $this->mUserId, $client_id, $year, $year]
            );

            foreach($dataset as $row)
            {
                $ta_dataset[$row->key . $year] = $row;
            }
        }
        return $ta_dataset;
    }

    private function getTaReportRange($client_id, $start_date, $end_date)
    {
        $dataset = DB::select("WITH turnover_percent AS (
                SELECT job_category, generation, replacement_cost, state_code
                FROM turnover_percent
            ),
            supported_states AS (
                SELECT DISTINCT state_code
                FROM turnover_percent
            ),
            employee_type_calc AS (
                SELECT DISTINCT ON (wp.identifier)
                wp.id,
                    wp.job_category,
                    CASE
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                        ELSE 'Other'
                    END AS generation,
                    wp.annual_salary,
                    CASE
                        WHEN (wp.end_date IS NOT NULL AND wp.end_date BETWEEN to_date(?,'MM/DD/YYYY') AND to_date(?,'MM/DD/YYYY')) THEN 1
                        ELSE 0
                    END AS turnover_flag,
                    CASE
                        WHEN (cl.state IN (SELECT state_code from supported_states)) THEN cl.state
                        ELSE 'DEFAULT'
                    END AS state
                FROM workforce_profile wp
                JOIN clients cl on wp.client_id = cl.id
                WHERE wp.user_id = ? AND wp.client_id = ?
                AND wp.start_date <= to_date(?,'MM/DD/YYYY')
                GROUP BY wp.id, wp.job_category, generation, cl.state
                ORDER BY wp.identifier, wp.created_at DESC
            )

            SELECT
                    replace(lower(concat(etc.job_category, etc.generation)),' ','') as key,
                    etc.job_category,
                    etc.generation,
                    AVG(etc.annual_salary * tp.replacement_cost) AS avg_replacement_cost,
                    CAST(SUM(etc.turnover_flag) AS decimal) / CAST(COUNT(*) AS DECIMAL) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                GROUP BY etc.job_category, etc.generation
                UNION
                SELECT
                    'overall' as key,
                    'overall' as job_category,
                    'all' as generation,
                    cast(SUM(etc.annual_salary * tp.replacement_cost) AS decimal) / cast(COUNT(*) AS decimal) AS avg_replacement_cost,
                    cast(SUM(etc.turnover_flag) AS decimal) / cast(COUNT(*) AS decimal) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                UNION
                SELECT
                    etc.job_category as key,
                    etc.job_category as job_category,
                    '' as generation,
                    cast(SUM(etc.annual_salary * tp.replacement_cost) AS decimal) / cast(COUNT(*) AS decimal) AS avg_replacement_cost,
                    cast(SUM(etc.turnover_flag) AS decimal) / cast(COUNT(*) AS decimal) AS turnover_percent,
                    SUM(etc.turnover_flag) AS turnover_count,
                    COUNT(*) as employee_count,
                    SUM(etc.annual_salary * tp.replacement_cost * etc.turnover_flag) AS replacement_cost
                FROM employee_type_calc etc
                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                    AND lower(tp.generation) = lower(etc.generation)
                    AND lower(tp.state_code) = lower(etc.state)
                GROUP BY etc.job_category
            ",
            [$start_date, $end_date, $this->mUserId, $client_id, $end_date]
        );

        foreach($dataset as $row)
        {
            $ta_dataset[$row->key] = $row;
        }

        return $ta_dataset;
    }

    // Deprecated
    private function getReplacementCost($year, $client_id)
    {
        $dataset = DB::select("WITH turnover_percent AS (
                SELECT job_category, generation, replacement_cost, state_code
                FROM turnover_percent
            ),
            supported_states AS (
                SELECT DISTINCT state_code
                FROM turnover_percent
            ),
            employee_turnover_end AS (
                SELECT DISTINCT ON (wp.identifier)
                    wp.id,
                    wp.job_category,
                    CASE
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                        ELSE 'Other'
                    END AS generation,
                    wp.annual_salary,
                    CASE
                        WHEN (cl.state IN (SELECT state_code from supported_states)) THEN cl.state
                        ELSE 'DEFAULT'
                    END AS state
                    FROM workforce_profile wp
                    JOIN clients cl on wp.client_id = cl.id
                    WHERE wp.user_id = ? and wp.client_id = ?
                    AND wp.end_date IS NOT NULL AND EXTRACT(YEAR FROM wp.end_date) = ?
                    GROUP BY wp.id
                    ORDER BY wp.identifier, wp.created_at DESC
                ),

                employee_turnover_start AS (
                    SELECT DISTINCT ON (wp.identifier)
                    wp.id,
                    wp.job_category,
                    CASE
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                        ELSE 'Other'
                    END AS generation,
                    wp.annual_salary,
                    CASE
                        WHEN (cl.state IN (SELECT state_code from supported_states)) THEN cl.state
                        ELSE 'DEFAULT'
                    END AS state
                    FROM workforce_profile wp
                    JOIN clients cl on wp.client_id = cl.id
                    FROM workforce_profile
                    WHERE wp.user_id = ? and wp.client_id = ?
                    AND wp.end_date IS NOT NULL AND EXTRACT(YEAR FROM wp.end_date) = ?
                    GROUP BY wp.id
                    ORDER BY wp.identifier, wp.created_at DESC
                ),

                employee_turnover AS (
                    select
                        (
                            COALESCE((
                                select SUM(etc.annual_salary * tp.replacement_cost)
                                from employee_turnover_start etc
                                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                                    AND lower(tp.generation) = lower(etc.generation)
                                    AND lower(tp.state_code) = lower(etc.state)
                            ), 0)
                            -
                            COALESCE((
                                select SUM(etc.annual_salary * tp.replacement_cost)
                                from employee_turnover_end etc
                                JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                                    AND lower(tp.generation) = lower(etc.generation)
                                    AND lower(tp.state_code) = lower(etc.state)
                            ), 0)
                        )
                        AS replacement_cost_savings,
                        COALESCE((
                            select SUM(etc.annual_salary * tp.replacement_cost)
                            from employee_turnover_start etc
                            JOIN turnover_percent tp on lower(tp.job_category) = lower(etc.job_category)
                                AND lower(tp.generation) = lower(etc.generation)
                                AND lower(tp.state_code) = lower(etc.state)
                        ), 0) AS previous_cost
                )

                SELECT replacement_cost_savings,
                    CASE
                        WHEN previous_cost <> 0 THEN CAST(replacement_cost_savings AS DECIMAL) / CAST(previous_cost AS DECIMAL)
                        ELSE 0
                    END AS retention_rate_change
                FROM employee_turnover
            ",
            [$this->mUserId, $client_id, $year, $this->mUserId, $client_id, $year-1]
        );

        return array_values($dataset)[0];
    }
}
