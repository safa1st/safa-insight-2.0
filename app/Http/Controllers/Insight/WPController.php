<?php

namespace App\Http\Controllers\Insight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Insight\ImportRequest;
use App\Models\Insight\Batch;
use App\Models\Insight\Lists;
use App\Models\Insight\WorkforceProfile;

use DB;
use Storage;
use DateTime;

class WPController extends Controller
{
    protected $batch;

    private $mClientId;
    private $mUserId;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id)
    {
        $this->mUserId = auth()->id();

        $dataset = DB::select("select b.id, b.client_id, b.title, b.created_at, count(wp.*) as total_records
            from batch b
            left outer join workforce_profile wp on b.id = wp.batch_id
            where b.user_id = ? and b.client_id = ?
            group by b.id, b.client_id, b.title, b.created_at",
            [$this->mUserId, $client_id]
        );

        $wp_delete_url = route('insight.dashboard.wp.delete', ['client_id' => $client_id]);

        $wp_upload_url = route('insight.dashboard.wp.upload', ['client_id' => $client_id]);

        return view('insight.dashboard.wp', compact('client_id', 'dataset', 'wp_delete_url', 'wp_upload_url'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list($client_id, $batch_id)
    {
        $this->mUserId = auth()->id();

        $dataset = DB::select("select
            b.title,
            wp.id, wp.identifier, wp.gender, wp.dob, wp.job_category, wp.job_title,
            wp.annual_salary, wp.start_date, wp.end_date,
            CASE
                WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                ELSE 'Other'
            END AS generation
            from workforce_profile wp
            join batch b on b.id = wp.batch_id
            where wp.batch_id = ?
            and wp.client_id = ?
            and wp.user_id = ?
            order by wp.id",
            [$batch_id, $client_id, $this->mUserId]
        );

        return view('insight.dashboard.wp_list', compact('client_id', 'batch_id', 'dataset'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(ImportRequest $request)
    {
        $this->mUserId = auth()->id();
        $this->mClientId = $request->get('client_id');
        $batch = $this->saveBatch($request);
        if (!$batch)
        {
            return redirect()->route('insight.dashboard.wp.home', $this->mClientId)->with([
                'flash_type' => 'alert-danger',
                'flash_message' => '<strong>Something went wrong!</strong>'
            ]);
        }

        $filePath = Storage::disk('client_data')->path('') . $batch->file['storage'];
        $fileFormat = $this->getFormat($filePath);
        if ($fileFormat == "full")
        {
            $this->saveFullCSVData($batch, $filePath);
        }
        else if ($fileFormat == "lite")
        {
            $this->saveLiteCSVData($batch, $filePath);
        }

        return redirect()
            ->route('insight.dashboard.wp.home', $this->mClientId)
            ->with([
                'flash_type' => 'alert-success',
                'flash_message' => '<strong>Complete!</strong> Data import successful.'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $status = 0;
        $this->mUserId = auth()->id();
        $client_id = $request->get('client_id');
        $batch_id = $request->get('batch_id');
        try {
            DB::beginTransaction();
            $batch = Batch::where('id', $batch_id)->where('user_id', $this->mUserId)->firstOrFail();
            $msg = "Successfully deleted data upload $batch->title";
            $batch->forceDelete();
            if (isset($batch->file))
                unlink(
                    env('CLIENT_STORAGE_DIR') .
                    DIRECTORY_SEPARATOR .
                    $batch->file['storage']
                );

        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            $status = 1;
            $msg = "Error Deleting Upload.";
        }
        DB::commit();

        return json_encode(['status' => $status, 'msg' => $msg]);
    }

    // Private Methods
    private function saveBatch(ImportRequest $request)
    {
        try {
            DB::beginTransaction();
            $batch = new Batch();
            $batch->user_id = $this->mUserId;
            $batch->title = $request->get('title');
            $batch->client_id = $this->mClientId;
            $list_result = Lists::where('type', 'batch_type')
                ->where('code', Lists::batch_type_WPU)
                ->get();
            if (count($list_result) > 0)
                $batch->batch_type_lid = $list_result[0]->id;
            else
            {
                return null;
            }

            $batch->file = [
                'title' => $request->get('title'),
                'filename' => $request->file('upload')->getClientOriginalName(),
                'storage' => $request->file('upload')->store('imports/'.$this->mUserId, 'client_data'),
                'size' => $request->file('upload')->getSize(),
                'ext' => $request->file('upload')->getClientOriginalExtension(),
                'mimeType' => $request->file('upload')->getMimeType(),
            ];
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return null;
        }
        $batch->save();
        DB::commit();
        return $batch;
    }

    private function getFormat($filePath)
    {
        try
        {
            $handle = fopen($filePath, "r");
            while (($row = fgetcsv($handle)) !== FALSE)
            {
                // Only Check the first row, allow it to fail in parsing of other rows
                $fieldCount = count($row);

                // 20 is the field count in the Full CSV
                if ($fieldCount == 20)
                    return 'full';
                // 8 is the field count in the Lite CSV
                if ($fieldCount == 8)
                    return 'lite';

                break;
            }
        } catch (Exception $e) {
            return "";
        }
        return "";
    }

    private function saveFullCSVData($batch, $filePath)
    {
        try {
            DB::beginTransaction();
            $handle = fopen($filePath, "r");
            $firstRowFlag = true;
            while (($row = fgetcsv($handle)) !== FALSE)
            {
                // Skip first row since it is supposed to be the header
                if($firstRowFlag) { $firstRowFlag = false; continue; }
                try {
                    $WP = new WorkforceProfile();
                    $WP->user_id = $this->mUserId;
                    $WP->client_id = $this->mClientId;
                    $WP->batch_id = $batch->id;
                    $WP->identifier = $row[0];
                    $WP->first_name = $row[1];
                    $WP->last_name = $row[2];
                    $gender = strtoupper($row[3]);
                    if ($gender != 'M' && $gender != 'F' && $gender != 'O')
                        $gender = 'O';
                    $WP->gender = $gender;
                    $WP->dob = $row[4];
                    $WP->address1 = $row[5];
                    $WP->address2 = $row[6];
                    $WP->city = $row[7];
                    $WP->state = $row[8];
                    $WP->zip = $row[9];
                    $WP->country = $row[10];
                    $WP->phone = $row[11];
                    $WP->mobile = $row[12];
                    $WP->email = $row[13];
                    $job_cat = strtolower($row[14]);
                    if ($job_cat != 'executive' && $job_cat != 'management' && $job_cat != 'staff')
                        $job_cat = 'staff';
                    $WP->job_category = $job_cat;
                    $WP->job_title = $row[15];
                    $WP->annual_salary = $row[16];
                    $WP->start_date = $row[17];
                    if ($row[18])
                        $WP->end_date = $row[18];
                    $WP->associations = $row[19];
                    $WP->save();
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    DB::rollback();
                }
            }


        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
        DB::commit();
    }

    private function saveLiteCSVData($batch, $filePath)
    {
        try {
            DB::beginTransaction();
            $handle = fopen($filePath, "r");
            while (($row = fgetcsv($handle)) !== FALSE)
            {
                try {
                    $WP = new WorkforceProfile();
                    $WP->user_id = $this->mUserId;
                    $WP->client_id = $this->mClientId;
                    $WP->batch_id = $batch->id;
                    $WP->identifier = $row[0];

                    // If there are any issues with the data, skip the row
                    if (count($row) != 8) continue;
                    if (!$this->validateDate($row[2])) continue;
                    if (!$this->validateDate($row[6])) continue;
                    if ($row[7] && !$this->validateDate($row[7])) continue;

                    $gender = strtoupper($row[1]);
                    if ($gender != 'M' && $gender != 'F' && $gender != 'O')
                        $gender = 'O';
                    $WP->gender = $gender;

                    $WP->dob = $row[2];
                    $job_cat = strtolower($row[3]);
                    if ($job_cat != 'executive' && $job_cat != 'management' && $job_cat != 'staff')
                        $job_cat = 'staff';
                    $WP->job_category = $job_cat;
                    $WP->job_title = $row[4];
                    $WP->annual_salary = $row[5];
                    $WP->start_date = $row[6];
                    if ($row[7])
                        $WP->end_date = $row[7];
                    $WP->save();
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    DB::rollback();

                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());

        }
        DB::commit();
    }

    private function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
}
