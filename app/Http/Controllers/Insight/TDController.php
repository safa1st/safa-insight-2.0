<?php

namespace App\Http\Controllers\Insight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class TDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id, $is_for_print = false)
    {
        $this->mUserId = auth()->id();

        $previous_url = route('insight.clientfocus.ta.home.summary', ['id' => $client_id]);

        $state = "DEFAULT";
        $flag_path = "";
        $this->getReportingLocation($client_id, $state, $flag_path);

        return view(
            'insight.clientfocus.td-home',
            compact('client_id', 'previous_url', 'is_for_print', 'state', 'flag_path')
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function td_replacement_cost(Request $request)
    {
        $this->mUserId = auth()->id();
        $client_id = $request->get('client_id');
        $rc_generation = $request->get('rc_generation');
        $rc_jobtype = $request->get('rc_jobtype');

        $rc_dataset = $this->getReplacementCost($client_id, $rc_generation, $rc_jobtype);
        return json_encode($rc_dataset);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function td_turnover_count(Request $request)
    {
        $this->mUserId = auth()->id();
        $client_id = $request->get('client_id');
        $tc_generation = $request->get('tc_generation');
        $tc_jobtype = $request->get('tc_jobtype');

        $tc_dataset = $this->getTurnoverCount($client_id, $tc_generation, $tc_jobtype);
        return json_encode($tc_dataset);
    }


    // Private Methods

    private function getReportingLocation($client_id, &$state, &$flag_path)
    {
        $dataset = DB::select("
            WITH supported_states AS (
                SELECT DISTINCT state_code FROM turnover_percent
            ),

            reporting_state AS (
                SELECT 
                CASE
                    WHEN (state IN (SELECT state_code from supported_states)) THEN state
                    ELSE 'DEFAULT'
                END AS state
                FROM clients
                WHERE user_id = ? and id = ?
            )

            SELECT l.state_name, l.flag_path
            FROM reporting_state rs
            JOIN location l ON l.state_code = rs.state
            ",
            [$this->mUserId, $client_id]
        );

        $state = "DEFAULT";
        $flag_path = "";
        // Will only be one row
        foreach($dataset as $row)
        {
            $state = $row->state_name;
            $flag_path = $row->flag_path;
        }
    }

    private function getReplacementCost($client_id, $rc_generation, $rc_jobtype)
    {
        $whereClause = ($rc_generation != "all") ? "and lower(replace(wp.generation, ' ', '')) = '$rc_generation'" : "";
        $whereClause .= ($rc_jobtype != "all") ? "and wp.job_category = '$rc_jobtype'" : "";

        $dataset = DB::select("WITH turnover_percent AS (
                    SELECT job_category, generation, replacement_cost, state_code
                    FROM turnover_percent
                ),
                supported_states AS (
                    SELECT DISTINCT state_code
                    FROM turnover_percent
                ),
            
                wp_clean AS (
                    SELECT wp.identifier as lid,
                        wp.id,
                        CASE
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                            WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                            ELSE 'Other'
                        END AS generation,
                        lower(wp.job_category) as job_category,
                        EXTRACT(YEAR FROM wp.end_date) AS end_year,
                        wp.annual_salary,
                        wp.created_at,
                        wp.user_id,
                        wp.client_id,
                        CASE
                            WHEN (cl.state IN (SELECT state_code from supported_states)) THEN cl.state
                            ELSE 'DEFAULT'
                        END AS state
                        FROM workforce_profile wp
                        JOIN clients cl on wp.client_id = cl.id
                        WHERE wp.end_date IS NOT NULL
                ),

                turnover_cost AS (
                    SELECT DISTINCT ON (wp.lid)
                        wp.id,
                        wp.generation,
                        wp.job_category,
                        wp.end_year,
                        SUM(wp.annual_salary * COALESCE(tp.replacement_cost, 0)) AS replacement_cost_per_person
                        FROM wp_clean wp
                        JOIN turnover_percent tp on lower(tp.job_category) = lower(wp.job_category)
                        AND lower(tp.generation) = lower(wp.generation)
                        AND lower(tp.state_code) = lower(wp.state)
                        WHERE wp.user_id = ? and wp.client_id = ?
                        $whereClause
                        GROUP BY wp.lid, wp.id, wp.generation, wp.job_category, wp.end_year
                ),
						
				year_range AS (
					SELECT
					generate_series(
						CAST(LEAST(MIN(EXTRACT(YEAR FROM end_date)), MIN(EXTRACT(YEAR FROM start_date))) AS integer),
						CAST(GREATEST(MAX(EXTRACT(YEAR FROM end_date)), MAX(EXTRACT(YEAR FROM start_date))) AS integer)
					) AS year
					FROM workforce_profile
					WHERE user_id = ? and client_id = ?
				)
								 
                SELECT SUM(COALESCE(tc.replacement_cost_per_person, 0)) replacement_cost, yr.year
                FROM year_range yr
				LEFT OUTER JOIN turnover_cost tc ON yr.year = tc.end_year
                group by yr.year
                order by yr.year
            ",
            [$this->mUserId, $client_id, $this->mUserId, $client_id]
        );

        return $dataset;
    }

    private function getTurnoverCount($client_id, $tc_generation, $tc_jobtype)
    {
        $whereClause = ($tc_generation != "all") ? "and lower(replace(generation, ' ', '')) = '$tc_generation'" : "";
        $whereClause .= ($tc_jobtype != "all") ? "and job_category = '$tc_jobtype'" : "";

        $dataset = DB::select("WITH 
            wp_clean AS (
                SELECT wp.identifier as lid,
                    wp.id,
                    CASE
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1995 AND EXTRACT(YEAR FROM wp.dob) <= 2012 THEN 'Gen Z'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1977 AND EXTRACT(YEAR FROM wp.dob) <= 1994 THEN 'Millennials'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1966 AND EXTRACT(YEAR FROM wp.dob) <= 1976 THEN 'Gen X'
                        WHEN EXTRACT(YEAR FROM wp.dob) >= 1946 AND EXTRACT(YEAR FROM wp.dob) <= 1965 THEN 'Baby Boomer'
                        ELSE 'Other'
                    END AS generation,
                    lower(wp.job_category) as job_category,
                    wp.end_date,
                    EXTRACT(YEAR FROM wp.end_date) AS end_year,
                    wp.created_at
                    FROM workforce_profile wp
                    JOIN clients cl on wp.client_id = cl.id
                    WHERE wp.end_date IS NOT NULL
                    AND wp.user_id = ? and wp.client_id = ?
            ),
            turnover AS (
                SELECT DISTINCT ON (lid)
                    id,
                    generation,
                    job_category,
                    end_year,
                    COUNT(end_date) AS turnover_count
                    FROM wp_clean
                    WHERE 1=1
                    $whereClause
                    GROUP BY lid, id, generation, job_category, end_year
            ),
						
            year_range AS (
                SELECT
                generate_series(
                    CAST(LEAST(MIN(EXTRACT(YEAR FROM end_date)), MIN(EXTRACT(YEAR FROM start_date))) AS integer),
                    CAST(GREATEST(MAX(EXTRACT(YEAR FROM end_date)), MAX(EXTRACT(YEAR FROM start_date))) AS integer)
                ) AS year
                FROM workforce_profile
                WHERE user_id = ? and client_id = ?
            )

            SELECT SUM(COALESCE(tc.turnover_count, 0)) as turnover_count, yr.year
            FROM year_range yr
			LEFT OUTER JOIN turnover tc ON yr.year = tc.end_year
            group by yr.year
            order by yr.year
            ",
            [$this->mUserId, $client_id, $this->mUserId, $client_id]
        );

        return $dataset;
    }
}
