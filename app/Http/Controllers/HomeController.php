<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        return view('insight.dashboard.home');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showAllClients()
    {
      $clients = Client::whereRaw('user_id = ?', [Auth::id()])->get();

      $create_client_url = route('insight.create.client');

      foreach ($clients as $client) {
        $client->homeURL = route('insight.clientfocus.home', ['client_id' => $client->id]);
        $client->company_logo = route('client.get.logo', ['client_id' => $client->id]);
      }

      return view('insight.dashboard.home', compact('clients', 'create_client_url'));
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function showCreateClient()
    {
      return view('insight.dashboard.addclient');
    }


}
