<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Http\Resources\ClientCollection;
use App\Http\Resources\Client as ClientResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return new ClientCollection(Client::where('user_id', Auth::id())->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'industry' => 'required',
          'size' => 'required',
          'address' => 'required',
          'city' => 'required',
          'state' => 'required',
          'zipcode' => 'required|digits:5',
          'contact.name' => 'required',
          'contact.email' => 'required|email',
          'contact.workPhone' => 'required|digits:10',
          'contact.mobilePhone' => 'required|digits:10',
        ]);

        $client = new Client;
        $client->user_id = Auth::id();
        $client->name = $request->input('name');
        $client->industry = $request->input('industry');
        $client->size = $request->input('size');
        $client->address = $request->input('address');
        $client->address2 = $request->input('address2');
        $client->city = $request->input('city');
        $client->state = $request->input('state');
        $client->zipcode = $request->input('zipcode');
        $client->contact_name = $request->input('contact.name');
        $client->contact_email = $request->input('contact.email');
        $client->contact_work_phone = $request->input('contact.workPhone');
        $client->contact_mobile_phone = $request->input('contact.mobilePhone');
        $client->save();

        return new ClientResource($client);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required',
        'industry' => 'required',
        'size' => 'required',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'zipcode' => 'required|digits:5',
        'contact.name' => 'required',
        'contact.email' => 'required|email',
        'contact.workPhone' => 'required|digits:10',
        'contact.mobilePhone' => 'required|digits:10',
      ]);

      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $id])->first();

      if (!$client){
        abort(404);
      }

      $client->name = $request->input('name');
      $client->industry = $request->input('industry');
      $client->size = $request->input('size');
      $client->address = $request->input('address');
      $client->address2 = $request->input('address2');
      $client->city = $request->input('city');
      $client->state = $request->input('state');
      $client->zipcode = $request->input('zipcode');
      $client->contact_name = $request->input('contact.name');
      $client->contact_email = $request->input('contact.email');
      $client->contact_work_phone = $request->input('contact.workPhone');
      $client->contact_mobile_phone = $request->input('contact.mobilePhone');
      $client->save();

      return new ClientResource($client);
    }

    /**
     * Return the logo
     *
     * @param  int  $client_id
     * @return \Illuminate\Http\Response
     */
    public function indexlogo($client_id)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      if (!$client){
        // open the file in a binary mode
        abort(404);
      }

      if (!$client->logo_path){
        // open the file in a binary mode
        return response()->file(public_path('/img/company_logo_missing.png'));
      }

      $logo = Storage::get($client->logo_path);

      return response($logo, 200)->header('Content-Type', $client->logo_mime_type);

    }

    /**
    * Store the logo
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $client_id
    * @return \Illuminate\Http\Response
    */
     public function storelogo(Request $request, $client_id)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $client_id])->first();

      if (!$client){
        abort(404);
      }

      //max size of 500kb
      $this->validate($request, [
        'file' => 'required|file|image|max:500',
      ]);

      $uploaded_file = $request->file('file');

      $img = Image::make($uploaded_file);
      $img->fit(300);

      $path = $uploaded_file->hashName('client_logos');
      Storage::put($path, (string) $img->encode('jpg'));

      $client->logo_path = $path;
      $client->logo_mime_type = $img->mime();
      $client->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $client = Client::whereRaw('user_id = ? and id = ?', [Auth::id(), $id])->first();

      if (!$client){
        abort(404);
      }

      $client->delete();
    }




}
