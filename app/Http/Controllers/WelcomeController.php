<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function show()
    {
        // Don't show welcome page, redirect directly to login page
        //return view('welcome');
        return redirect('/login');
    }
}
