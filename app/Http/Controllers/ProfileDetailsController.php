<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the user's profile details.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            //'name' => 'required|max:255',
            'address' => 'max:255',
            'city' => 'max:255',
            'state' => 'max:255',
            'zip' => 'max:255',
            'country' => 'max:255',
            'businessphone' => 'max:255',
            'mobilephone' => 'max:255',
        ]);

        $request->user()->forceFill([
            //'name' => $request->name,
            //'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'country' => $request->country,
            'businessphone' => $request->businessphone,
            'mobilephone' => $request->mobilephone
        ])->save();
    }
}