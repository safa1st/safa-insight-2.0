<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => (string)$this->id,
            'name'      =>  $this->name,
            'industry'  => $this->industry,
            'size'      => $this->size,
            'address'   => $this->address,
            'address2'   => is_null($this->address2) ? '' : $this->address2,
            'city'      => $this->city,
            'state'     => $this->state,
            'zipcode'   => $this->zipcode,
            'contact'   => [
                'name' => $this->contact_name,
                'email' => $this->contact_email,
                'workPhone' => $this->contact_work_phone,
                'mobilePhone' => $this->contact_mobile_phone,
            ],
            'editPath'    => route('client.update', ['id' => $this->id]),
            'logo'    => route('client.get.logo', ['id' => $this->id]),
            'home'    => route('insight.clientfocus.home', ['client_id' => $this->id]),
        ];
    }
}
