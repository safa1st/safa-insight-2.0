<?php

namespace App\Http\Requests\Insight;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ImportRequest
 *
 * This class validates the Insight import form
 *
 * @package App\Http\Requests\Insight
 */
class ImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'upload' => [
                'required',
                'file'
            ],
            'title' => 'required'
        ];
    }
}
